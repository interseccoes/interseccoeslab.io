---
title: Ficha Técnica
layout: default
---

<div class="container-fluid">
  <div class="row">
<div class="ficha-tecnica col-sm-10">
  <h1>Ficha Técnica</h1>
  <p><b><a href="#" target="_blank">Produção e Curadoria:</a></b></p>
  <p> Andrea Campos de Sá, Artur Cabral e Lynn Carone</p>
  <br>
  <p><b><a href="#" target="_blank">Equipe de produção:</a></b></p>
  <p>Lindemberg Vieira <br>
    Marina Dutra<br>
    Paulo Henrique Valeriano Miranda<br>
</p>
  <br>
  <p><b><a href="#" target="_blank">Apoio:</a></b></p>
  <p>Adriana Teixeira<br>
Ariel Rovo<br>
Bárbara Gomes de Lima Moreira<br>
Denize Passos<br>
Kevin Jonathan<br>
Lysia Ramos Costa Pessoa<br>
Matheus Antonio Vieira<br>
 Morgana Taro<br>
 Machado<br>
</p>
  <br>
  <p><b><a href="#" target="_blank">Artistas:</a></b></p>
  
  <p>
    <a><b></b></a>
    <span>
Adriana Teixeira Machado <br>
Alana Avohay<br>
Alvaro Cardoso de Santana<br>
Amanda de Figueiredo<br>
Amanda Morais Silva<br>
Amelly Tschiedel <br>
Ana Beatriz Messias<br>
Ana Flávia Mendes Vieira<br>
Ana Rosa Nabuco<br>
Andreas Kneip<br>
Ângelo Delico Nunes<br>
Ariel Rovo<br>
Bárbara Gomes de Lima Moreira<br>
Bianca Magalhaes Baeza Carneiro<br>
Brida Abajur<br>
Bruno Cantelmo<br>
Catarina Leite<br>
Cleia Chaves dos Santos Scheibler<br>
Daniele Rodrigues de Sousa<br>
Danilo Teodorio Rodrigues da Silva<br>
Eloísa Rodrigues<br>
Felippe Mohr<br>
Francisco Dalcastagne Miguel<br>
Gabriel Barbosa Soriano de Carvalho<br>
Gabriela Barreto Daldegan<br>
Henrique Farage<br>
Isadora Carvalho<br>
Jessica Ribeiro Ferreira<br>
João Lucas Cavalcante Vieira <br>
João Pedro Nunes Vasconcelos<br>
João Ricardo(Cob4lto)<br>
Jonas Gabriel Flores Matos<br>
Joyce Sant’Anna<br>
Lais Lidice Santana da Silveira<br>
Léa Juliana dos Santos Ribeiro<br>
Lindemberg Vieira<br>
Luana Silva<br>
Luiz Ferreira<br>
Lysia Ramos Costa Pessoa<br>
Maiara Martins<br>
Maria Clara Rodrigues de Oliveira<br>
Mariana Rezende Monteiro<br>
Marina Bianchetti Rodrigues da Costa (Gumubu)<br>
Marina Dutra<br>
Matheus Antonio Vieira<br>
Morgana Taro<br>
Paula Vieira<br>
Paulo Arthur Castro<br>
Paulo Henrique Valeriano Miranda<br>
Pedro Henrique Nascimento (Feio)<br>
Pralads<br>
Ruana Carla Santiago de Andrade<br>
Sandri Oliveira<br>
Sergio Soares Pedreira<br>
Thyeme Guedes da Silva Figueiredo<br>
Victória Reis<br>
    </span>
  </p>
</div>
</div>
</div>


