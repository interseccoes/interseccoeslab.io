---
layout: obra
thumbnail: /assets/tumbnail/t-desmanche.png
turma: Arte Eletrônica
title: desmanche 
artista: Eloísa Rodrigues
instagram: https://www.instagram.com/eloartera
texto-descricao: Uma animação que estuda movimentos, luzes e formatos dentro de um cômodo, produzido por meio do programa Blender 3D. No vídeo, os elementos do quarto se desmancham quando a câmera se movimenta, deixam de ser concretos.
bio: Nascida em Brasília, estudante do 5º semestre de Artes Visuais na UnB, ilustradora e, desde recentemente, exploradora do universo da arte 3D.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/8zZ7krlLRxU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </p>