---
layout: obra
thumbnail: /assets/tumbnail/t-entreoartificialeoreal.png
title: Série - Entre o artificial e o real
artista: Amanda Morais
turma: Materiais em Arte II
instagram: https://www.instagram.com/mandiocamorais
texto-descricao: Céu ou mar? Água ou ar? O mar só é azul porque a cor é um reflexo do céu. Uma obra de acrílica de anos atrás inspirada no mar é jogada na água e o céu se reproduz na pintura. Eu formo nuvens artificiais na piscina para recriar um céu palpável, e acabo me perdendo entre o real e o postiço. O papel azul secando depois de mergulhado me lembra tanto o céu quanto o mar. O reflexo do céu na piscina e na pintura potencializam a criação de um universo sintético, trazendo à tona a metalinguagem da série. O céu no mar real ou o mar no céu artificial.
bio: Amanda é artista experimental e Bacharelanda em Artes Visuais na UnB. Busca através de seu trabalho explorar todas as formas e linguagens possíveis dentro das artes. Ama principalmente a pintura, as intervenções e gerar incômodos reflexivos e críticas através de suas obras. Acredita na arte como ferramenta de luta, ativismo e conscientização. Já realizou pesquisas com o LEME (Laboratório Experimental de Materiais Expressivos), explorando a ecoprodução e o reaproveitamento de materiais nas artes visuais.
---
<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      <li data-target="#imagens" data-slide-to="5"></li>
      <li data-target="#imagens" data-slide-to="6"></li>
      <li data-target="#imagens" data-slide-to="7"></li>


    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/materiais/amanda/imagem1.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/amanda/imagem2.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/amanda/imagem3.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/amanda/imagem4.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/materiais/amanda/imagem5.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/materiais/amanda/imagem6.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/materiais/amanda/imagem7.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      </div>
    
    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/materiais/amanda/imagem1.jpg"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/materiais/amanda/imagem2.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/amanda/imagem3.jpg"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/materiais/amanda/imagem4.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/amanda/imagem5.jpg"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/materiais/amanda/imagem6.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/amanda/imagem7.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>

</div>