---
layout: obra
thumbnail: /assets/tumbnail/t-monotipia.png
title: Monotipia 
artista: Bianca Baeza
turma: Materiais em Arte II
instagram: https://www.instagram.com/biancabaeza
texto-descricao: Impressões feitas com matriz de vidro (espelho) e tinta para gravura solúvel em água. Coleta de folhas e matos da rua, escritas apontando pensamentos, dúvidas e inquietações momentâneas.
bio: Bianca Baeza é estudante de Artes Visuais pela UnB. Seu trabalho se desenvolve principalmente em textos, som e linguagem visual. Ela não sabe muito bem como falar de si, acha muito complicado (e estranho falar sobre si mesma em 3ª pessoa). Os trabalhos dessa exposição foram vagando nesse conflito de entendimento de si.
---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      <li data-target="#imagens" data-slide-to="5"></li>
      <li data-target="#imagens" data-slide-to="6"></li>
      <li data-target="#imagens" data-slide-to="7"></li>
      <li data-target="#imagens" data-slide-to="8"></li>


    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/materiais/bianca/imagem1.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/bianca/imagem2.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/bianca/imagem3.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/bianca/imagem4.png" alt="{{ page.title }}" class="img-fluid">
      </div>
     <div class="carousel-item">
        <img src="/assets/obras/materiais/bianca/imagem5.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/bianca/imagem6.png" alt="{{ page.title }}" class="img-fluid">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/materiais/bianca/imagem7.jpeg" alt="{{ page.title }}" class="img-fluid">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/materiais/bianca/imagem8.jpeg" alt="{{ page.title }}" class="img-fluid">
      </div>
      </div>
       

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/materiais/bianca/imagem1.jpg"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/materiais/bianca/imagem2.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/bianca/imagem3.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/bianca/imagem4.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/bianca/imagem5.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/bianca/imagem6.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/bianca/imagem7.jpeg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/bianca/imagem8.jpeg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
 
</div>
