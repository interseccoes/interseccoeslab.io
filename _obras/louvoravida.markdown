---
layout: obra
thumbnail: /assets/tumbnail/t-louvoravida.png
title: Louvor a vida
artista: Sérgio Pedreira
turma: Materiais em Arte II
instagram: https://www.instagram.com/sergiopedreiraartesvisuais
texto-descricao: Elementos da natureza inspiram o artista visual Sérgio Pedreira, em investigações que reúne nesta exposição virtual. A água ganha dimensão sagrada e o fogo aparece com viés divino, na mostra de seus novos trabalhos, que usam a fotografia como suporte. Numa de suas investigações artísticas, ele visitou o Parque Olhos D'Água onde fez fotos primeiro em uma fonte; depois, seguiu para um outro ponto do espaço, por cima da lagoa do sapo. Sempre lembrando de momentos vividos quando era criança, quando bebia água da nascente que ficava no meio da serra.Em outra experimentação artística, Sérgio explorou um tema incandescente; o fogo que existe nos ambientes, assim como no interior de cada ser vivo. A chama que faz com que a vida seja preservada, na reprodução de espécies, na sexualidade humana. A cor vermelha surge, como símbolo de ciclos biológicos, de retomadas. Vermelho paixão.
bio: Sérgio Pedreira-1966-Campina Grande-PB. Estudante de Artes Visuais-Licenciatura pela Universidade de Brasília-UnB. Arquiteto e urbanista formado pela FAUFBA. Trabalha com argila e com queimas em alta temperatura, está descobrindo com muito prazer a expressividade de outros materiais ao mesmo tempo rompendo limites.
---
<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators" style="right: 135px;">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      

    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/materiais/sergio/imagem1.JPG" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/sergio/imagem2.JPG" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/sergio/imagem3.JPG" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/sergio/imagem4.JPG" alt="{{ page.title }}" class="img-fluid">
      </div>
      </div>
    
    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next" style="right: 173px;">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/materiais/sergio/imagem1.JPG"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
    <img src="/assets/obras/materiais/sergio/imagem2.JPG" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
    <img src="/assets/obras/materiais/sergio/imagem3.JPG"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
    <img src="/assets/obras/materiais/sergio/imagem4.JPG" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
</div>