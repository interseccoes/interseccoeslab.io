---
layout: obra
thumbnail: /assets/tumbnail/t-anotacoesexperienciaserelatosdoselementosnocarcere.png
title: Anotações, experiências e relatos dos elementos no cárcere.
artista: Gabriel Barbosa Soriano de Carvalho
turma: Materiais em Arte II
instagram: https://www.instagram.com/angmarts
texto-descricao: Ao longo do semestre foram realizados muitos projetos, foram momentos de muita tentativa de me desvincular do que faço com maior domínio, que é o campo do desenho artístico, para a criação de obras com materiais e objetos totalmente fora de minha área de controle. Porém, apesar de que foram recursos e materiais nada convencionais para o meu tipo de trabalho, a atmosfera mística de meus projetos sempre estiveram presentes durante o processo criativo, dando um ar de mistério às obras. O processo de criação incentivou novos horizontes no tipo de pesquisa que estava em progresso e ao mesmo foi um desafio pessoal, pois a área do desenho exige um certo controle de execução. Em suma, todo o processo de trabalhos e discussões com a Professora Lynn Carone foram ótimos, pois com a tutoria dela, fiz uma expansão em minha visão de pesquisa em relação a materialidade da poética.
bio: Gabriel Soriano, 1990 – Brasília – DF. Estudante de artes visuais na Universidade de Brasília – Unb. Envolvido com projetos e estudos na área de desenho, pintura e literatura no campo da fantasia e estudos do misticismo. Dá aulas em ateliê na área de anatomia fantástica e pintura clássica. Nesta época de pandemia, está envolvido com podcasts sobre desenho.
---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      <li data-target="#imagens" data-slide-to="5"></li>
      <li data-target="#imagens" data-slide-to="6"></li>
      <li data-target="#imagens" data-slide-to="7"></li>

    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/materiais/gabriel/imagem1.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/gabriel/imagem2.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/gabriel/imagem3.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/gabriel/imagem4.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
     <div class="carousel-item">
        <img src="/assets/obras/materiais/gabriel/imagem5.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/gabriel/imagem6.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/gabriel/imagem7.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      </div>


    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/materiais/gabriel/imagem1.jpg"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/materiais/gabriel/imagem2.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/gabriel/imagem3.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/gabriel/imagem4.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/gabriel/imagem5.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/gabriel/imagem6.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/gabriel/imagem7.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
</div>
