---
layout: obra
thumbnail: /assets/tumbnail/t-maladie.png
title: Maladie
artista: Paulo Arthur Castro
turma: Introdução à Gravura
instagram: https://www.instagram.com/pauloarthur
texto-descricao: As técnicas propostas ao longo do semestre foram combinadas à linguagem de interesse do artista, que estabelece relação entre gravura e vídeo através da projeção de imagens e silhuetas, isolamento dos objetos e distorção de reflexos para compor um experimento audiovisual surrealista
bio: Paulo Arthur Castro nasceu em Brasília, 2001. Mora na cidade mais antiga do Distrito Federal e cursa Artes Visuais na Universidade de Brasília. Apesar de criar a partir de técnicas tradicionais de desenho e fotografia, seu interesse principal se encontra na intersecção entre as artes visuais e cênicas, usando o próprio corpo como suporte para revelar imagens.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/5JSHY9Kd7Ao" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </p>

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      

    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/gravura/paulo/imagem1.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/paulo/imagem2.JPG" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/paulo/imagem3.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      </div>
      
   
       

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/gravura/paulo/imagem1.jpg"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/gravura/paulo/imagem2.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/paulo/imagem3.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  
 
</div>
