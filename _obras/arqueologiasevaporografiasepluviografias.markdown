---
layout: obra
thumbnail: /assets/tumbnail/t-arqueologiasevaporografiasepluviografias.png
title: Arqueologias, Evaporografias e Pluviografias
artista: Felippe Mohr
turma: Introdução à Gravura
instagram: https://www.instagram.com/felippemohr
texto-descricao: Para presente exposição, apresento três séries distintas de experimentações que realizei no campo da gravura expandida. Iniciamos com a série “Arqueologia”, onde expandi o conceito da gravura para o ato de imprimir a forma de um objeto sobre uma superfície, como uma pegada na areia. Utilizei objetos colecionados de viagens (uma máscara e 2 esculturas) pressionando-os sobre uma superfície grossa de fubá mimoso compactado. O resultado visual me remeteu a imagens arqueológicas entalhadas em pedra. Na série de evaporografias, experimentei as marcas que a evaporação de água salgada deixaria sobre uma superfície. Para tanto utilizei papel cartão colorido como superfície, imergindo-o numa solução de aquosa com alta concentração de sal. Após o processo de evaporação, crostas de sal se formaram, remetendo à superfície de planetas. Por fim, a série “Chove no Cerrado” foi criada a partir de uma técnica onde uno a fotografia e a monotipia. Fotografei algumas árvores do cerrado, imprimi as fotografias em impressora jato de tinta, depositei essa “matriz” com a foto impressa virada sobre uma folha de papel com alta gramatura e coloquei o conjunto na chuva. A água, ao molhar a matriz, fez com que a tinta da imagem impressa passasse para o papel suporte, resultando numa imagem aguada, remetendo à aquarela.
bio: Felippe Mohr nasceu em 1984, em Joinville-SC e atualmente cursa Teoria, Crítica e História da Arte na Universidade de Brasília – UNB. Seu interesse maior é pela pintura e fotografia, mas descobriu nas aulas de gravura novas formas artísticas de experimentação.
---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      <li data-target="#imagens" data-slide-to="5"></li>
      <li data-target="#imagens" data-slide-to="6"></li>
      <li data-target="#imagens" data-slide-to="7"></li>
      <li data-target="#imagens" data-slide-to="8"></li>


    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/gravura/felippe/imagem1.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/felippe/imagem2.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/felippe/imagem3.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/felippe/imagem4.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
     <div class="carousel-item">
        <img src="/assets/obras/gravura/felippe/imagem5.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/felippe/imagem6.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/gravura/felippe/imagem7.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/felippe/imagem8.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
   
      </div>

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/gravura/felippe/imagem1.jpg"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/gravura/felippe/imagem2.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/felippe/imagem3.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/felippe/imagem4.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/felippe/imagem5.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/felippe/imagem6.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/felippe/imagem7.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/felippe/imagem8.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
 
</div>
