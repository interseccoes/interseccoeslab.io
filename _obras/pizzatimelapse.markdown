---
layout: obra
thumbnail: /assets/tumbnail/t-pizzatimelapse.png
turma: Animação
title: pizza time (-lapse)
artista: Pralads
instagram: https://www.instagram.com/pralade
texto-descricao: Stop motion com técnicas híbridas, receita de pizza, senso de humor duvidoso e quebra de expectativas.
bio: técnico de artista, artista eletrônico e investigador de curiosidades e piadas bobas. Busca o discurso absurdo e as possibilidades dentro da experimentação técnica nas artes.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/oEXEgO54nqE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </p>