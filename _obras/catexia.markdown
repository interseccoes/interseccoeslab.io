---
layout: obra
thumbnail: /assets/tumbnail/t-catexia.png
turma: Arte Eletrônica
title: Catexia
artista: Maiara Martins
instagram: https://www.instagram.com/orphiria
texto-descricao: Catexia é uma animação em 2D cutout, produzida em diferentes técnicas como desenho, pintura digital e colagem digital, sobre um momento de angústia e luto. Adentrando no significado do título, Catexia é o processo pelo qual a energia libidinal disponível na psique é vinculada a ou investida na representação mental de uma pessoa, ideia ou coisa. Quando o indivíduo passa pelo processo de luto, o desinteresse pelas ocupações normais e a preocupação com o recente finado é interpretado por estudos psicanalíticos como uma retirada de libido dos relacionamentos habituais o que gera uma extrema catexia na pessoa perdida.
bio: Maiara Martins é formada em Publicidade e Propaganda pela UnB (2017) e atualmente cursa Artes Visuais-bacharelado pela mesma instituição. Está descobrindo e investigando sua poética (e mitopoética) por meio da Arte Psicánalise.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/LI4duf0_WGM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </p>