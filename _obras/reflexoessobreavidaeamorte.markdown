---
layout: obra
thumbnail: /assets/tumbnail/t-reflexoessobreavidaeamorte.png
turma: Arte Eletrônica
title: Reflexões sobre a vida e a morte
artista: Daniele Rodrigues de Sousa
instagram: https://www.instagram.com/adesenheira
texto-descricao: Reflexões sobre a vida e a morte, não é fácil pensar sobre essas questões, ainda mais em tempos tão difíceis no qual estamos vivendo. Observamos tantas perdas, entretanto é preciso refletir. A vida e a morte estão conectadas, muitas vezes não gostamos de pensar sobre a morte e admito é ruim, complexo e doloroso pensar nela, mas é necessário pois assim como a vida ela faz parte da nossa existência na terra.
bio: Daniele Rodrigues de Sousa, graduanda em artes visuais pela Universidade de Brasília. Ilustradora, o desenho sempre esteve presente em seu processo artístico. Em suas criações busca explorar o corpo, o cotidiano e a natureza, dentro de técnicas artísticas como; a pintura, escultura, gravura, fotografia etc. É através das artes que consegue expressar seus sentimentos e emoções.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/SZoGMelSsLE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </p>