---
layout: obra
thumbnail: /assets/tumbnail/t-esperandooamanhachegar.png
title: Esperando o amanhã chegar
artista: Luana Silva
turma: Materiais em Arte II
instagram: https://www.instagram.com/_lua.art https://www.instagram.com/llua_nna 
texto-descricao: A máscara de algodão representa não só o ar, mas também a falta dele quando utilizamos de fato a máscara, ou metaforicamente com toda essa situação.  A venda feita de galhos, representa os olhos fechados para as queimadas que ainda estão ocorrendo mas pararam de ser comentadas. Assim como as mortes, que antes eram números, mas hoje não têm sido mais tão divulgadas.  Usando essa metamorfose quis evidenciar alguns sentimentos que o isolamento me causou. As pessoas que têm o poder podem usar ele de muitas formas para manipular uma sociedade inteira, e hoje consigo ver claramente essa situação nas mídias. 
bio: Luana Silva, 2001. Brasília, DF. Estudante de Artes Visuais pela Universidade de Brasília-UnB. Experienciando várias técnicas e se descobrindo com o artista desde o início do curso. Atualmente tem realizado seus trabalhos permeando os temas relacionados com ansiedade.

---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/materiais/luana/imagem1.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/luana/imagem2.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/luana/imagem3.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
    </div>
    
    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/materiais/luana/imagem1.jpg"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/materiais/luana/imagem2.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/luana/imagem3.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
</div>
