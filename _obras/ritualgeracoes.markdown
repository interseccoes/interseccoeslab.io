---
layout: obra
thumbnail: /assets/tumbnail/t-ritualgeracoes.png
turma: Arte Eletrônica
title: Ritual Gerações
artista: Léa Juliana
instagram: https://www.instagram.com/_leajuliana
texto-descricao: Sinto-me de volta ao ventre, sinto o calor da água que me mantém viva, fecho os olhos e começo a sentir as suas mãos acariciarem meus cabelos, a energia se intensifica e sinto as mãos das suas mães e mais as das mães delas, o processo continua até nos enxergarmos infinitas. Nesse momento, a energia volta a fluir e chega novamente ao ventre. Sinto-me forte, pronta para levantar, posso agora derramar essa água para que ela fertilize o solo aos meus pés. Imediatamente ela alcança o chacra estrela e, em seguida, o centro da terra. A energia se expande do cosmo ao centro e do centro ao cosmo. Já sou uma mulher, guardo no meu corpo a energia das que me antecederam, como forma de gratidão, me pinto, como se as pintassem também. Honro o feminino de cada uma delas e tudo que elas fizeram e que me permitiram estar vivendo esse momento agora. Passado e presente no mesmo tempo, lugar e energia. Entrego em oferenda o fogo da transmutação.
bio: Nascida em Salvador/Ba, graduada em Administração de Empresas, pela UcSal, atualmente, estuda Artes Visuais, na UnB. Realiza rituais, estuda sua ancestralidade, desenha, fotografa e pinta sonhos e intuições. Se cura por meio da arte.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/2s5dLIEKtKw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </p>