---
layout: obra
thumbnail: /assets/tumbnail/t-36graus.png
turma: Animação
title: 36 graus
artista: Brida Abajur 
instagram: https://www.instagram.com/abajurr
texto-descricao: animação experimental utilizando filmadora de fita cassete
bio: Brida Abajur vive e trabalha em Brasília. Dedica-se à expressão de narrativas tragicômicas e ao emprego de stop motion no terror trash.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/EVWe904PyH0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </p>
