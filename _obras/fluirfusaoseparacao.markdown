---
layout: obra
thumbnail: /assets/tumbnail/t-fluirfusaoseparacaoserieaguatinta.png
title: “Fluir, fusão, separação - série água tinta”
artista: Bárbara Moreira
turma: Materiais em arte II
instagram: https://www.instagram.com/barbara.moreira.arte
texto-descricao: No movimento de criar com presença, o fluir e coincidir das tintas líquidas, suas fusões e divergências podem ser observadas, inspirando relações e movimentos para a vida. A série “Água Tinta - Fluir, fusão, separação” nos arrebata para o onírico, o descanso visual e mental, a pairar nas cores e nas danças realizadas entre os líquidos. 
bio: Bárbara Moreira (1987) é designer se aprofundando nas Artes Visuais. Seus trabalhos tomam forma a partir das falas do inconsciente, em um processo livre e expressivo. Relaciona memória, espaço e natureza no processo de criação das suas obras se utilizando de pintura, fotografia e performance. É membro do Coletivo Matriz em Brasília/DF, com trabalho apresentado na zine “ciclo #1 . o tempo circular” do projeto Baleia/DF. Participou das exposições “Matriz”, no Museu Nacional (2019), em Brasília/DF e (In)Tangível - Matéria Sensível, virtual pelo instagram (2020).

---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      <li data-target="#imagens" data-slide-to="5"></li>
      <li data-target="#imagens" data-slide-to="6"></li>
      <li data-target="#imagens" data-slide-to="7"></li>
      <li data-target="#imagens" data-slide-to="8"></li>


    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/materiais/barbara/imagem1.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/barbara/imagem2.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/barbara/imagem3.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/barbara/imagem4.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
     <div class="carousel-item">
        <img src="/assets/obras/materiais/barbara/imagem5.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/barbara/imagem6.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/materiais/barbara/imagem7.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/materiais/barbara/imagem8.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      </div>

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/materiais/barbara/imagem1.jpg"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/materiais/barbara/imagem2.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/barbara/imagem3.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/barbara/imagem4.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/barbara/imagem5.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/barbara/imagem6.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/barbara/imagem7.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/barbara/imagem8.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
 
</div>
