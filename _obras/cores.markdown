---
layout: obra
thumbnail: /assets/tumbnail/t-cores.png
turma: Animação
title: Cores 
artista: Amelly Tschiedel 
instagram: https://www.instagram.com/amellytschiedel 
texto-descricao: “Cores” é uma animação 2D, com a duração de 50 segundos, que representa de forma lúdica o processo criativo da artista, em que não há planejamento prévio e o trabalho é formado pouco a pouco durante todo seu desenvolvimento. 
bio: Amelly é estudante de Artes Plásticas na UnB e sua principal pesquisa é sobre a cor dentro do âmbito da pintura a óleo e seu trabalho é focado mais em pinturas e desenhos.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/xNyFsabX6kg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </p>