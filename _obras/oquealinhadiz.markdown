---
layout: obra
thumbnail: /assets/tumbnail/t-oquealinhadiz.png
title: o que a linha diz 
artista: Maria Clara Rodrigues de Oliveira
turma: Materiais em Arte II
instagram: https://www.instagram.com/m_clara_28
texto-descricao: sentimentos expostos pelo mínimo da linha 
bio: Maria Clara Rodrigues de Oliveira nasceu em 2001, é aluna de artes visuais na universidade de Brasília, tem afinidade com técnicas de desenho e arte digital com interesse em fotografia.

---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      <li data-target="#imagens" data-slide-to="5"></li>
=
    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/materiais/maria/imagem1.PNG" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/maria/imagem2.PNG" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/maria/imagem3.PNG" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/maria/imagem4.PNG" alt="{{ page.title }}" class="img-fluid">
      </div>
     <div class="carousel-item">
        <img src="/assets/obras/materiais/maria/imagem5.PNG" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/maria/imagem6.PNG" alt="{{ page.title }}" class="img-fluid">
      </div>
       </div>
       

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"  style="background-image: url(&quot;data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='%23000' width='8' height='8' viewBox='0 0 8 8'%3e%3cpath d='M5.25 0l-4 4 4 4 1.5-1.5L4.25 4l2.5-2.5L5.25 0z'/%3e%3c/svg%3e&quot;);"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon" style="background-image: url(&quot;data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='%23000' width='8' height='8' viewBox='0 0 8 8'%3e%3cpath d='M2.75 0l-1.5 1.5L3.75 4l-2.5 2.5L2.75 8l4-4-4-4z'/%3e%3c/svg%3e&quot;);"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/materiais/maria/imagem1.PNG"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/materiais/maria/imagem2.PNG" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/maria/imagem3.PNG" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/maria/imagem4.PNG" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/maria/imagem5.PNG" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/maria/imagem6.PNG" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
</div>
