---
layout: obra
thumbnail: /assets/tumbnail/t-dancadaagua.png
title: Dança da água 
artista: Thyeme Figueiredo
turma: Materiais em Arte II
instagram: https://www.instagram.com/euthyeme
texto-descricao:  Um experimento entre vários, buscando observar e entender como a simbologia e o conceito do termo "água" se relacionam com a água propriamente dita, suas cores, seu movimento...
bio: Thyeme Figueiredo, 1999, Belém-PA. Designer de interiores e estudante de Artes Visuais na Universidade de Brasília - UNB. Atualmente desenvolve projetos de design, diagramação, ilustração e encadernação para o Diário Estranho, enquanto experimenta diversas técnicas de criação e expressão artísticas, desde aquarela até vídeo arte.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/jamrzl3M_Jg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>