---
layout: obra
thumbnail: /assets/tumbnail/t-duzentosesetenta.png
title: duzentos e setenta
artista: Paulo Valeriano
turma: Materiais em Arte II
instagram: https://www.instagram.com/paulllos
texto-descricao: Buscando explorar questões do tempo, do espaço, do corpo e de suas relações desenvolvi uma série de cianotipias. A série é composta por três conjuntos de cianótipos, onde o processo de exposição ao sol se deu de forma a permitir a intervenção do acaso, como movimentações causadas pelo vento ou pela chuva, interrupções na fonte de luz, etc. O trabalho se relaciona também com o momento de isolamento social. Nesses muitos meses em casa, o tempo se tornou um tema de contemplação, neles também, coletei folhas que caem de uma árvore no quintal, duzentas e setenta folhas, nove meses em casa. O que me interessa nesse trabalho são principalmente as relações entre o tempo e o espaço, o eu e o outro. Perder-se em um momento suspenso, que não por isso deixa de sofrer as intempéries do tempo. Se propor a estar suspenso no tempo, e não por isso deixar de ser catalisador de causas e consequências. Pairando sobre rastros que o acaso deixa para trás, essa série de trabalhos se encontra na imprecisão e na incerteza, na conformação e na ação, ou na não-ação.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/zCgQvDOoPwM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
     


    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/materiais/paulo/imagem1.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/paulo/imagem3.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/materiais/paulo/imagem4.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      </div>
   
       

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/materiais/paulo/imagem1.jpg"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/materiais/paulo/imagem3.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/paulo/imagem4.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  
 
</div>
