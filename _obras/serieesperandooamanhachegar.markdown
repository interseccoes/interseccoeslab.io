---
layout: obra
thumbnail: /assets/tumbnail/t-serieesperandooamanhachegar.png
title: Carapaça
artista: Luana Silva
turma: Introdução à Gravura
instagram: https://www.instagram.com/_lua.art
texto-descricao: Essa série foi desenvolvida ao longo das aulas de gravura. Começou com um estêncil de uma árvore e alguns carimbos. A partir disso consegui desenvolver a série “carapaça”, que tem como principal objetivo estabelecer uma conexão entre corpo e natureza. Na obra conseguimos ver uma monotipia de uma casca de árvore e do meu próprio braço, ao lado de uma casca de árvore e de uma casca de besouro. Pensando no momento que estamos vivendo com o uso de máscaras fica difícil identificar quem são as pessoas por trás delas, o mesmo acontece com nossas próprias cascas.
bio: Luana Silva-2001-Brasilia, Df. Estudante de Artes Visuais pela Universidade de Brasília UnB. Experienciando várias técnicas e se descobrindo com o artista desde o início do curso. Atualmente tem realizado seus trabalhos permeando os temas relacionados com ansiedade.

---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
     


    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/gravura/luana/imagem1.JPG" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/luana/imagem2.JPG" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/luana/imagem3.JPG" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/luana/imagem4.JPG" alt="{{ page.title }}" class="img-fluid">
      </div>
      </div>
       

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/gravura/luana/imagem1.JPG"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/gravura/luana/imagem2.JPG" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/luana/imagem3.JPG" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/luana/imagem4.JPG" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
 
</div>
