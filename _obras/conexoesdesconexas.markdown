---
layout: obra
thumbnail: /assets/tumbnail/t-conexoesdesconexas.png
title: Conexões desconexas
artista: Cléia Mesmo
turma: Materiais em Arte II 
instagram: https://www.instagram.com/cleiamesmo
texto-descricao: Esse trabalho em tricô procura questionar o quanto o ser humano está  emaranhado nos meandros da tecnologia, principalmente  neste período de quarentena.
bio: Cléia Chaves dos Santos Scheibler, 51 anos, é estudante de Artes Visuais (bacharelado) na UnB. Desenvolve investigações com materiais variados e inusitados, utilizando técnicas da costura (à máquina) e artesanato. Atualmente está envolvida em duas pesquisas que envolvem a pintura, o desenho e a escultura, a partir do emprego de descarte da indústria têxtil e utilização das técnicas de cestaria, respectivamente.
---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      <li data-target="#imagens" data-slide-to="5"></li>
      <li data-target="#imagens" data-slide-to="6"></li>

    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/materiais/cleia/imagem1.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/cleia/imagem2.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/cleia/imagem3.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/cleia/imagem4.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
           <div class="carousel-item">
        <img src="/assets/obras/materiais/cleia/imagem5.JPG" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/cleia/imagem6.JPG" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/cleia/imagem7.JPG" alt="{{ page.title }}" class="img-fluid">
      </div>
      </div> 
    
    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/materiais/cleia/imagem1.jpg"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/materiais/cleia/imagem2.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/cleia/imagem3.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/cleia/imagem4.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/cleia/imagem5.JPG"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/materiais/cleia/imagem6.JPG" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/cleia/imagem7.JPG" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
</div>
