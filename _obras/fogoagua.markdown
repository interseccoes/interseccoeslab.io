---
layout: obra
thumbnail: /assets/tumbnail/t-fogoagua.png
title: fogo_agua
artista: João Lucas Cavalcante Vieira
turma: Materiais em Arte II
instagram: https://www.instagram.com/jlucas_cavalcante
texto-descricao: O que me interessou no processo de criação foi, num primeiro momento, contrapor uma característica mais orgânica que acontece na produção de um desenho em aquarela, com o controle proporcionado pelo uso do grafite. Apesar da tentativa inicial de uma representação pictórica da contraposição de duas técnicas, a aquarela, feita de forma mais livre, junto ao desenho em grafite, que descreve uma espécie de comportamento de onda, criaram uma ideia de complementação; os desenhos tornaram-se uma representação imagética da água. Por fim, concluí que somente o fogo poderia contradizer o que os desenhos exprimiam. 
bio: João Lucas é estudante de Artes Visuais pela Universidade de Brasília. Desenvolve seu trabalho principalmente na linguagem do desenho. Na primeira metade do ano de 2020, desenvolveu as obras “Luz-tempo” e “Quebra”, expostas no mês de julho do mesmo ano, na exposição virtual (In) tangível.

---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      
    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/materiais/joao/imagem1.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/joao/imagem2.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/joao/imagem3.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       </div>
       
       

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/materiais/joao/imagem1.jpg"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/materiais/joao/imagem2.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/joao/imagem3.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  
 
</div>
