---
layout: obra
thumbnail: /assets/tumbnail/t-idoloproibido.png
title: Ídolo Proibido (Trans-Humanidade)
artista: Morgana Taro
turma:  Introdução à Gravura
instagram: https://www.instagram.com/yearsbeforecris
texto-descricao: Do seio de Lilith, nasceu a mais ambiciosa criatura. Esta criatura frágil, descendente direta da divindade laçou pelo pescoço os males do mundo e fez da terra, e de tua mãe, tua montaria. Esse ser Lógico, Racional, tecnológico, conquistou por meio da ciência sua própria divindade. Com o dom da criação e da destruição, se tornou Deus por tecnologia. O ser virtual, que do seio se nutriu, encapsula a maternidade e sua origem divina, Subjuga a mãe terra pela tecnologia, se torna não mais carne, e sim aço. O divino Tecnológico.O seio de Lilith, fonte da divindade, é matéria prima energética para o novo maquinário tecnológico de sua prole. Onde a humanidade assume nova forma, entre o aço do progresso e a carne de sua origem, o ser fluido se divide dentro da nova carne, na Trans-Humanidade. O ídolo proibido é um símbolo do triunfo da humanidade, mas é, também, um lembrete da culpa humana. Ao erradicar sua progenitora, conquistou sua divindade, e também o seu abandono. Deuses por força própria, sozinhes por ganância.
bio: Nascida em 1999, natural de Governador Valadares, Morgana Taro (ou apenas TARO) é artista e animadora 3D, Musicista e Game designer. Com foco interativo, seu trabalho tende a quebrar as barreiras obra-espectador, de forma a unir o mundo real com o virtual, e criar a familiaridade dentro do desconhecido. Com obras interativas apresentadas na BRING (mostra brasiliense de indie games) e em exposições como a Encruzilhada, sua produção confronta o espectador com situações surreais e por vezes abstratas, gerando a contemplação visual pela intriga e pela vontade, na textura, na cor e no sensorial.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/qdTeXinOdg4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>


    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/gravura/morgana/imagem2.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/morgana/imagem3.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/morgana/imagem4.png" alt="{{ page.title }}" class="img-fluid">
      </div>
      </div>
       

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/gravura/morgana/imagem2.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/morgana/imagem3.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/morgana/imagem4.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
 
</div>
