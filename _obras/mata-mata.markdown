---
layout: obra
thumbnail: /assets/tumbnail/t-mata-mata.png
title: mata-mata
artista: Marina Dutra
turma: Introdução à Gravura
instagram: https://www.instagram.com/marinadutrag
texto-descricao: uma série sobre a interrupção do ciclo em que se tem uma ideia, você mata essa ideia e ela te mata de volta, pois ela persiste.
bio: 21 anos, estudante de artes visuais e psicologia. busca em suas experimentações artísticas investigar as relações do sujeito, tendo a transparência do suporte como parte da poética.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/HZG8wrcJJdo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>
 