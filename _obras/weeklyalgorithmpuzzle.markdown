---
layout: obra
thumbnail: /assets/tumbnail/t-weeklyalgorithmpuzzle.png
turma: Arte Eletrônica
title: weekly algorithm puzzle
artista: João Pedro Nunes Vasconcelos
instagram: https://www.instagram.com/virt.u
texto-descricao: As composições são frutos do exercício de digerir o cotidiano, verbalização de processos dolorosos para criar caminhos mais fáceis de se enxergar.
bio: Nascido em Brasília, estudante de artes, foco na arte como ferramenta de comunicação e verbalização.
---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      <li data-target="#imagens" data-slide-to="5"></li>
      <li data-target="#imagens" data-slide-to="6"></li>


    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/ae/joaopedro/imagem1.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/ae/joaopedro/imagem2.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/ae/joaopedro/imagem3.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/ae/joaopedro/imagem4.png" alt="{{ page.title }}" class="img-fluid">
      </div>
     <div class="carousel-item">
        <img src="/assets/obras/ae/joaopedro/imagem5.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/ae/joaopedro/imagem6.png" alt="{{ page.title }}" class="img-fluid">
      </div>
      </div>
       

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/ae/joaopedro/imagem1.png"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/ae/joaopedro/imagem2.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/ae/joaopedro/imagem3.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/ae/joaopedro/imagem4.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/ae/joaopedro/imagem5.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/ae/joaopedro/imagem6.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
 
</div>
