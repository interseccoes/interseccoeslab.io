---
layout: obra
thumbnail: /assets/tumbnail/t-reminiscencias.png
turma: Arte Eletrônica
title: reminiscências
artista: victória reis
instagram: https://www.instagram.com/peledereis
texto-descricao:  série que aborda o resgate de memórias ambientadas, com apoio visual e auditivo. se apresenta como uma história sendo contada a partir de uma perspectiva pessoal, e das pessoas que compõem as lembranças.
bio: artista visual, natural de Brasília. sua produção, pensada a partir da cor, material e suporte, tem como linguagem principal o autorretrato. assim, trabalha as questões da sua identidade e vivência enquanto mulher negra de pele clara, por meio, em especial, da pintura.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/ZRwAUk0Tn6U" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>
 