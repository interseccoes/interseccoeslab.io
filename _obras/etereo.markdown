---
layout: obra
thumbnail: /assets/tumbnail/t-etereo.png
title: Etéreo
artista: Ariel Rovo Cerqueira
turma: Materiais em Arte II
instagram: https://www.instagram.com/rovoartwork
texto-descricao: O projeto foi desenvolvido durante as aulas de Materiais em Arte II, no qual foi utilizado a mitologia e a ilustração como pano de fundo, juntamente com materiais e temas propostos. Como o principal campo do artista é a ilustração e a manipulação de imagens, seu principal objetivo foi criar um campo místico juntamente com o real.
bio: Ariel Rovo, 2001, Brasília, DF. Estudante de Artes Visuais pela Universidade de Brasília-UnB. Atualmente experienciando várias técnicas no curso, mas seu  principal campo de trabalho é a ilustração. Trabalha com temas envolvendo o campo da fantasia.
---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
    


    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/materiais/ariel/imagem1.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/ariel/imagem2.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      </div>
       

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/materiais/ariel/imagem1.jpg"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/materiais/ariel/imagem2.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
 
</div>
