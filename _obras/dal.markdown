---
layout: obra
thumbnail: /assets/tumbnail/t-dal.png
turma: Animação
title: Dal
artista: Angelo Delico Nunes
instagram: https://www.instagram.com/cruelseraph
texto-descricao: animação experimental
bio:  ngelo Delico Nunes nasceu em Salvador-BA e se mudou para Brasília em 2006. Apaixonado por desenho e pela arte.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/RdzZBmKsFqY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </p>
