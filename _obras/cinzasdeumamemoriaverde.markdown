---
layout: obra
thumbnail: /assets/tumbnail/t-cinzasdeumamemoriaverde.png
title: Cinzas de uma memória verde
artista: Maria Clara Rodrigues de Oliveira
turma: Introdução à Gravura
instagram: https://www.instagram.com/m_clara_28
texto-descricao: Através do carvão a artista trás a melancolia da enorme perda da flora e fauna brasileira caudadas pelas queimadas no pantanal no ano de 2020.
bio: Maria Clara Rodrigues de Oliveira nasceu em 2001, é aluna de artes visuais na universidade de Brasília, tem afinidade com tecnicas de desenho e arte digital com interesse em fotografia. Maria Clara Rodrigues de Oliveira nasceu em 2001, é aluna de artes visuais na universidade de Brasília, tem afinidade com tecnicas de desenho e arte digital com interesse em fotografia.
---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      <li data-target="#imagens" data-slide-to="5"></li>


    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/gravura/maria/imagem1.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/maria/imagem2.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/maria/imagem3.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/maria/imagem4.png" alt="{{ page.title }}" class="img-fluid">
      </div>
     <div class="carousel-item">
        <img src="/assets/obras/gravura/maria/imagem5.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       
      </div>
       

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/gravura/maria/imagem1.png"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/gravura/maria/imagem2.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/maria/imagem3.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/maria/imagem4.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/maria/imagem5.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
 
</div>
