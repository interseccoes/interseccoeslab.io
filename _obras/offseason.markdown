---
layout: obra
thumbnail: /assets/tumbnail/t-offseason.png
title: Offseason
artista: Henrique Farage
turma: Introdução à Gravura
instagram: https://www.instagram.com/henriquefarage
texto-descricao: A temática é baseada na relação entre o basquete e a arte, já explorada na matéria de Materiais em Arte 1. A gravura permite traçar um paralelo entre as repetições realizadas no ato de imprimir com as repetições dos movimentos no esporte, ambos em busca de um resultado que independe da perfeição, mas depende da semelhança. A exploração desse conceito se baseou principalmente em técnicas como a monotipia e o carimbo, porém também abrange para o campo do desenho. O que intitula essa série é o período de recesso da NBA, entre o final de uma temporada e o início da próxima. Atualmente estamos nesse período da temporada da NBA e é também como eu interpreto o momento atual de isolamento social. Apesar de não ter jogado, o basquete foi companheiro para o artista em muitos momentos durante a pandemia.
bio: Henrique Farage, 2000, Brasília, Brasil.Henrique nasceu e cresceu em Brasília. Graduado em Design Gráfico pelo IESB em 2019 e estudante de Artes Visuais desde 2018. Decidiu estudar as duas faculdades concomitantemente, tendo em vista que os conhecimentos adquiridos seriam complementares. A relação entre essas duas áreas cria conflitos diretamente relacionados ao processo criativo. Apesar de ter uma base no desenho, o estudo de diferentes técnicas de criação contribui para o amadurecimento do artista que está se direcionando às interações da arte com o esporte.
---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      <li data-target="#imagens" data-slide-to="5"></li>
      <li data-target="#imagens" data-slide-to="6"></li>


    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/gravura/henrique/imagem1.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/henrique/imagem2.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/henrique/imagem3.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/henrique/imagem4.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
     <div class="carousel-item">
        <img src="/assets/obras/gravura/henrique/imagem5.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/henrique/imagem6.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      </div>
       

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/gravura/henrique/imagem1.jpg"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/gravura/henrique/imagem2.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/henrique/imagem3.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/henrique/imagem4.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/henrique/imagem5.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/henrique/imagem6.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
 
</div>
