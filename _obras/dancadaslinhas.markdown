---
layout: obra
thumbnail: /assets/tumbnail/t-dancadaslinhas.png
turma: Arte Eletrônica
title: Dança das linhas
artista: Ana Flávia Mendes Vieira
instagram: https://www.instagram.com/flavieira.arte
texto-descricao: A artista utilizou de uma performance de dança escultura, que é uma união da dança com o trabalho artesanal crochê para criar a animação por meio de vídeos, fotografia e desenho digital. O trabalho se trata de uma dança onde não se sabe quem é que conduz, se é o corpo ou se é a linha, os movimentos é uma luta constante com o material. O trabalho sempre está em eterna construção, o trabalho só existe e é completo enquanto à movimento e a interação.
bio: Ana Flávia Mendes Vieira utilizou da técnica de animação rotoscopia através de imagens fotográficas e vídeos e desenho digital.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/1_KSgxlVfvQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </p>