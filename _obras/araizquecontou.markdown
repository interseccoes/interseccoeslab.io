---
layout: obra
thumbnail: /assets/tumbnail/t-araizquecontou.png
title: A Raiz Que Contou
artista: Joyce Sant’Anna Crisóstomo
turma: Introdução à Gravura
instagram: https://www.instagram.com/heyjoy.png
texto-descricao: Em uma viagem aos contos enraizados nas terras brasileiras, histórias são mais uma vez recontadas através da linguagem visual. Os trabalhos foram feitos com carimbos de borracha, tinta de carimbo e tinta acrílica.
bio: Joyce Sant’Anna é estudante de Licenciatura em Artes Visuais, tendo cursado antes o curso de Gestão Ambiental que estimulou dentro de si a paixão pela cultura brasileira e comunidades tradicionais. É fotógrafa, ilustradora e desbravadora da vida nas horas vagas. 
---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      <li data-target="#imagens" data-slide-to="5"></li>
      <li data-target="#imagens" data-slide-to="6"></li>


    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/gravura/joyce/imagem1.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/joyce/imagem2.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/joyce/imagem3.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/joyce/imagem4.png" alt="{{ page.title }}" class="img-fluid">
      </div>
     <div class="carousel-item">
        <img src="/assets/obras/gravura/joyce/imagem5.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/joyce/imagem6.png" alt="{{ page.title }}" class="img-fluid">
      </div>
   
      </div>

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/gravura/joyce/imagem1.png"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/gravura/joyce/imagem2.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/joyce/imagem3.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/joyce/imagem4.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/joyce/imagem5.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/joyce/imagem6.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
 
</div>
