---
layout: obra
thumbnail: /assets/tumbnail/t-fuligemdoafeto.png
ttitle: Fuligem do Afeto
artista: Ana Beatriz Messias                   
turma: Introdução à Gravura
instagram: https://www.instagram.com/inkbia           
texto-descricao: Série de obras feitas com Cliché Verre, técnica do final século XIX em que a matriz é o vidro, utilizando uma vela a artista passa a matriz sobre a chama formando tons de preto e cinza com a fuligem formada na superfície de vidro. Na escolha das matrizes, objetos de vidro encontrados na cozinha, espaço de abrigo afetivo, onde permite a mistura memória e imaginação, que recebem a retirada da fuligem formando a imagem e para a finalização da obra foi usada a técnica da fotografia digital. O resultado é a mistura da matéria preta com as sombras que formam a duplicidade da matriz.
bio: Ana Beatriz Messias, brasiliense, nascida em 1994, estudante de Artes Visuais na Universidade de Brasília. Tem afinidade pela pesquisa no campo das artes e educação. Em suas produções visuais transita por várias técnicas e é por meio dessas experimentações que cria possibilidades poéticas em suas obras. 
---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      <li data-target="#imagens" data-slide-to="5"></li>


    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/gravura/ana/imagem1.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/ana/imagem2.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/ana/imagem3.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/ana/imagem4.png" alt="{{ page.title }}" class="img-fluid">
      </div>
     <div class="carousel-item">
        <img src="/assets/obras/gravura/ana/imagem5.png" alt="{{ page.title }}" class="img-fluid">
      </div>
      </div>
   
       

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/gravura/ana/imagem1.png"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/gravura/ana/imagem2.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/ana/imagem3.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/ana/imagem4.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/ana/imagem5.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
   
</div>
