---
layout: obra
thumbnail: /assets/tumbnail/t-surfboards.png
title: Surfboards
artista: Sandri Oliveira
turma: Introdução à Gravura
instagram: https://www.instagram.com/oksandri
texto-descricao: Vivendo de angústias durante a pandemia no ano de 2020 me joguei em fazer uma visita a minha melhor amiga que mora em Florianópolis. Sentia que precisava passar por esse momento perto do mar e viver esse sonho de morar com ela. Melhores amigas de infância, morando juntas, sozinhas em uma Ilha. A ideia era passar um mês, fui ficando na cidade e o surf me chamou, na verdade já me chamava a muito tempo. Tinha feito algumas aulas com 6 anos e agora com 20 revivi vários momentos e me entreguei de corpo e alma. Surf é terapia, meditação, aonde acesso inúmeras emoções de diferentes intensidades. A exposição é um pequeno retrato do que vivi esses dias, com muito amor, no mar e em cima de uma prancha de surf.Trabalhos feitos a partir de stencil e tinta guache preta. Todos em 21cm X 29.7cm
bio: De Alagoas para o mundo, retrato o que sinto e vivo.
---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      <li data-target="#imagens" data-slide-to="5"></li>
      <li data-target="#imagens" data-slide-to="6"></li>
      <li data-target="#imagens" data-slide-to="7"></li>



    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/gravura/sandri/imagem1.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/sandri/imagem2.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/sandri/imagem3.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/sandri/imagem4.png" alt="{{ page.title }}" class="img-fluid">
      </div>
     <div class="carousel-item">
        <img src="/assets/obras/gravura/sandri/imagem5.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/sandri/imagem6.png" alt="{{ page.title }}" class="img-fluid">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/gravura/sandri/imagem7.png" alt="{{ page.title }}" class="img-fluid">
      </div>
      </div>
       

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/gravura/sandri/imagem1.png"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/gravura/sandri/imagem2.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/sandri/imagem3.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/sandri/imagem4.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/sandri/imagem5.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/sandri/imagem6.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/sandri/imagem7.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
 
</div>
