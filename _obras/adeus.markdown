---
layout: obra
thumbnail: /assets/tumbnail/t-adeus.png
title: Adeus
artista: Mariana Rezende Monteiro 
turma: Materiais em Arte II 
instagram: https://www.instagram.com/mrm_art_
texto-descricao: Estava chovendo com ventos fortes, era de tarde e o ar estava gelado… por um acaso olhei para fora e vi que tinha uma borboleta se segurando na grade, como quem procura abrigo no meio da tempestade. Ela ficou lá, se segurando na grade até a chuva parar e quando a chuva parou, ela saiu voando. Adeus. Uma cigarra na parede no meio da noite, sem cantar. Se movendo bem devagar ao invés de voar. Era como observar uma pessoa idosa andando, se movendo lentamente e sem pressa. Ela não foi embora. A cigarra andou de uma ponta para outra, demorou horas e eu a deixei ali, pois não estava cantando. Quando a manhã surgiu a cigarra estava no chão, morta. Adeus.
bio: Mariana Rezende Monteiro nasceu em 2000 e é estudante de Artes Visuais (licenciatura) na UnB. Desenvolve várias experimentações no campo da arte, mas as técnicas que mais tem afinidade são a pintura e o desenho. Recentemente descobriu um grande interesse pelos registros do cotidiano e de devaneios através da vídeo arte, que são o foco de sua pesquisa no momento.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/Cj6KeeGSl_A" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>