---
layout: obra
thumbnail: /assets/tumbnail/t-pegadasnochaodetaco.png
title: Pegadas no Chão de Taco 
artista: Lindemberg Vieira
turma: Introdução à Gravura
instagram: https://www.instagram.com/deliriumboy
texto-descricao: Uma relação intimista de um estado atual, uma vivência isolada em quatro paredes, registros de pensamento, danças, inquietações, caminhadas, preocupações, mix de sentimentos confinados em um quarto. A partir do formato da sola do próprio pé usado como modelo, são criados dois carimbos e deles surge a série Pegadas no Chão de Taco, com impressões sobre papel e animação Stop Motion, Lindemberg Vieira perpetua os próprios rastros de trânsito em seu quarto retangular, que, devido ao isolamento social causado pelo momento de pandemia, é o local que mais convive.
bio: Lindemberg de Sena Vieira, nascido em Brasília - DF, 1994. A.k.a Berg é Estudante de Artes Visuais pela Universidade de Brasília - UnB,  arteiro desde que se recorda e vem explorando em diversos campos das artes como no desenho, colagem, escultura, dentre outros. Em meio à produções de multilinguagens que discutem sobre arte narrativa, passando por inspirações na Op-art e Arte Psicodélica, participou em 2019 da exposição ‘’Arte em processo” na ADUnB, em 2020  participou da exposição virtual “INST.ANTE”, recentemente se dedica na produção de trabalhos em técnicas de gravura. 
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/5paCF6qRc8w" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
     


    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/gravura/lindemberg/imagem1.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/lindemberg/imagem2.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/lindemberg/imagem3.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/lindemberg/imagem4.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      </div>
   
       

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/gravura/lindemberg/imagem1.jpg"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/gravura/lindemberg/imagem2.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/lindemberg/imagem3.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/lindemberg/imagem4.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  
 
</div>
