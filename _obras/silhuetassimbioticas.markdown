---
layout: obra
thumbnail: /assets/tumbnail/t-silhuetassimbioticas.png
title: silhuetas simbióticas 
artista: Ariel Rovo Cerqueira
turma: Introdução à Gravura 
instagram: https://www.instagram.com/rovoartwork
texto-descricao: Um processo amadurecido ao longo do último mês. Feito digitalmente, procurei juntar plantas e fotografias para gerar silhuetas um tanto quanto monstruosas que se diferenciam das memórias carinhosas que eu tinha das mesmas.
bio: Ariel Rovo, 2001, Brasília, DF. Estudante de Artes Visuais pela Universidade de Brasília. Atualmente experienciando várias técnicas no curso, mas seu  principal campo de trabalho é a ilustração. Trabalha com temas envolvendo o campo da fantasia.

---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      <li data-target="#imagens" data-slide-to="5"></li>
      <li data-target="#imagens" data-slide-to="6"></li>


    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/gravura/ariel/imagem1.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/ariel/imagem2.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/ariel/imagem3.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/ariel/imagem4.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
     <div class="carousel-item">
        <img src="/assets/obras/gravura/ariel/imagem5.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/ariel/imagem6.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      </div>
   
       

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/gravura/ariel/imagem1.jpg"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/gravura/ariel/imagem2.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/ariel/imagem3.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/ariel/imagem4.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/ariel/imagem5.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/ariel/imagem6.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
 
</div>
