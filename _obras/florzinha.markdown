---
layout: obra
thumbnail: /assets/tumbnail/t-florzinha.png
turma: Animação
title: Florzinhas
artista: Marina Bianchetti Rodrigues da Costa (Gumubu)
texto-descricao: Florzinhas é um ballet animado dividido em três atos. Tenta explorar de forma lúdica um dos conceitos básicos da teoria das cores&#58; A junção de duas cores primárias, no caso amarelo e vermelho, gera uma cor secundária; laranja. A primeira cena apresenta a espécie de flores amarelas, a segunda a espécie vermelha e a última mostra a interação entre as duas, que gera novas espécies; a espécie laranja e a espécie vermelho-amarela.O curta é um trabalho de claymation feito com o aplicativo Stop Motion Studio e o programa Premier. A composição sonora foi feita por Ra. Trindade utilizando um teclado e o DAW Fruit Loops.
bio: Marina é estudante de artes plásticas na Universidade de Brasília e de animação na escola ICS. Se interessa principalmente pelo potencial que o movimento tem para suscitar a ilusão de vida.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/9bCU-tzXivQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </p>