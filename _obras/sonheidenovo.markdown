---
layout: obra
thumbnail: /assets/tumbnail/t-sonheidenovo.png
turma: Arte Eletrônica
title: Sonhei de novo...
artista: Jessica Ribeiro Ferreira
instagram: https://www.instagram.com/ajescae
texto-descricao: Uma animação 2D breve sobre a conversa entre uma menina e um coelho imaginário
bio: Estudante de artes visuais na unb desde o primeiro semestre de 2018, que as vezes tem alguns sonhos melancólicos.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/i_2ZmVAT2cA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </p>