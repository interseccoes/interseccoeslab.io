---
layout: obra
thumbnail: /assets/tumbnail/t-aguasoniricas.png
turma: Animação
title: Águas Oníricas
artista: Gabriela Barreto Daldegan
instagram: https://www.instagram.com/daldegangabriela
texto-descricao: Diante da subjetividade que o universo onírico carrega, as águas se apresentam como sua matéria prima no embalar do diálogo entre diferentes mundos. A partir do eterno efêmero, urge o convite da entrega.
bio: Artista experimental da vida, transeunte de alguns mundos, busco encaixar minha arte (eu) onde cabe e precisa caber.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/FdI-tQgeZ-U" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </p>