---
layout: obra
thumbnail: /assets/tumbnail/t-turuturu.png
turma: Arte Eletrônica
title: Turu, turu
artista: Ana Rosa Nabuco
instagram: https://www.instagram.com/coisasquesepassam
texto-descricao: O stop motion procura fazer uma alusão aos processos imagéticos da associação livre, conta-se a narrativa do nascimento, do crescimento e do desamparo que cada ser humano sente ao experienciar a individualidade.
bio: sou artista independente, nasci e cresci no meio rural no município de alto paraíso-go, meu discurso e direcionado a minha experiência como mulher cis genera e a simbologia desse papel, busco através do meu trabalho uma reinterpretação de mim e de minhas vivências. o aspecto lúdico,da cosmografia infantil e a estética da abstração são meus objetivos concientes. atualmente curso artes visuais na UNB.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/9IDjP6VIzwQ" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </p>