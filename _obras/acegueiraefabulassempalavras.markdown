---
layout: obra
thumbnail: /assets/tumbnail/t-acegueiraefabulassempalavras.png
title: “A cegueira e fábulas sem palavras”.
artista: A.F.
turma: Introdução à Gravura 
instagram: https://www.instagram.com/a.f.fmocreia
texto-descricao: Nas obras “A cegueira” trago colunas de revistas que não podem ser lidas pelo caos que as palavras se encontram. A ideia é causar essa cegueira para quem tenta ler o artigo na revista usando carimbo. Em “fábulas sem palavras”, me aproprio das fotos do Mac Adams encontrada em uma revista e crio minha própria linguagem narrativa inserindo carimbos nelas.
bio: A artista Amanda de Figueiredo assina seus trabalhos pelas iniciais do seu nome A.F. Nascida e crescida em Brasília, especificamente no Núcleo Bandeirante. Sempre gostou muito de criar e se expressar através das diversas formas artísticas. É estudante de Artes Visuais na UnB, mas seu coração é dividido pela literatura, cinema e artes visuais. Tenta mesclar no seu trabalho essas transdisciplinaridade em sua linguagem. Estuda licenciatura em artes e sua pesquisa na iniciação científica é voltada para arte educação e cultura visual decolonial. Já sua pesquisa artística ainda está em experimentação, porém já observa que procura abordar em suas obras diversas narrativas usando o lirismo para questionar a realidade na vida cotidiana.
---
<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      

    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/gravura/amanda/imagem1.jpg"  class="img-fluid">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/gravura/amanda/imagem2.jpg"  class="img-fluid mx-auto d-block">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/gravura/amanda/imagem3.jpg"  class="img-fluid mx-auto d-block">>
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/gravura/amanda/imagem4.jpg"  class="img-fluid">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/gravura/amanda/imagem5.jpg"  class="img-fluid mx-auto d-block">
      </div>

    </div>
    
    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>


<div class="d-block d-sm-none">
  <img src="/assets/obras/gravura/amanda/imagem1.jpg"  class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/amanda/imagem2.jpg"  class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/amanda/imagem3.jpg"  class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/amanda/imagem4.jpg"  class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/amanda/imagem5.jpg"  class="img-fluid" width="35%"><br><br>
  

</div>
