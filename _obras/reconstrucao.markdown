---
layout: obra
thumbnail: /assets/tumbnail/t-reconstrucao.png
title: reconstrução 
artista: Danilo Teodorío 
turma: Materiais em Arte II
instagram: https://www.instagram.com/theodoriio
texto-descricao: Preencho uma parte que se foi, gravando uma memória que havia ali.
bio: Danilo Teodorío, nasceu em 1995 na cidade de sobradinho/DF. Apreciador de técnicas manuais desde a infância, apaixonou-se pela costura e o artesanato. Estudante de Artes Visuais na Universidade de Brasília - UNB, sempre esteve envolvido área de Moda e Arte, buscando relação entre os dois universos. Desenvolve trabalhos com tecidos e linhas. Explora a experimentação com materiais do seu cotidiano, agregando um olhar para o desenvolvimento de cada obra .
---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators" style="right: 126px;">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      <li data-target="#imagens" data-slide-to="5"></li>

    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/materiais/danilo/imagem1.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/danilo/imagem2.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/danilo/imagem3.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/danilo/imagem4.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/danilo/imagem5.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/danilo/imagem6.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      </div>
    
    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next" style="right: 154px;">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
    <img src="/assets/obras/materiais/danilo/imagem1.jpg"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
    <img src="/assets/obras/materiais/danilo/imagem2.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
    <img src="/assets/obras/materiais/danilo/imagem3.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
    <img src="/assets/obras/materiais/danilo/imagem4.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
     <img src="/assets/obras/materiais/danilo/imagem5.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
    <img src="/assets/obras/materiais/danilo/imagem6.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>

</div>


