---
layout: obra
thumbnail: /assets/tumbnail/t-moveme.png
turma: Animação
title: MOVE ME
artista: João Ricardo(Cob4lto)
instagram: https://www.instagram.com/jrsouzacruz
texto-descricao: experimento em áudio e animação 2d
bio: artista multi-midia trabalhando como tatuador em Brasília. Produtor de peças audiovisuais e auditivas, sempre busca integrar a produção do gif e do glitch e mimetizar seu emocional visualmente.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/a0hCV3-QQnY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </p>