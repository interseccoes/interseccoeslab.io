---
layout: obra
thumbnail: /assets/tumbnail/t-beijoefogo.png
title: Beijo e fogo.
artista: Lysia Ramos Costa Pessoa
turma: Materiais em Arte II
instagram: https://www.instagram.com/lya_arte.ilustra
texto-descricao: Vídeo sobre relação metafórica do elemento fogo com a sensualidade e a libido. O desenho num papel A4 de duas figuras humanas de gênero não definido estão envolvidas numa relação carnal e são queimadas pelo fogo enquanto se desintegram com o papel.
bio: Lysia Ramos nasceu em 1999 em Recife, PE e atualmente cursa Teoria, Crítica e História da Arte pela Universidade de Brasília – UnB.Desenvolveu seu trabalho na linguagem do desenho com a utilização de materiais artísticos mais tradicionais e eventualmente produziu também uma série de trabalhos utilizando o corpo humano como suporte.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/KSnIrHwc0Fw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>