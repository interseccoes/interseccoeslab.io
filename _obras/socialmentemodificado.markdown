---
layout: obra
thumbnail: /assets/tumbnail/t-socialmentemodificado.png
title: Socialmente Modificado 
artista: Catarina Leite de Matos e Almeida
turma: Materiais em Arte ll 
instagram: #
texto-descricao: Fazendo o uso da linha como material a obra visa fazer uma demonstração de como a sociedade nos faz perder a forma natural da fisionomia humana, por meio de uma deformação do corpo e atitudes extremas, para assim se encaixar em padrões irreais criados pelas grandes mídias
bio: Catarina Leite de Matos e Almeida nasceu em 2001, em Salvador - BA, e atualmente está cursando Artes Visuais Licenciatura na Universidade de Brasília (UNB). Apresenta mais afinidade com técnicas voltadas para o desenho, mas também se interessa por fazer uso de técnicas como a fotografia e a vídeo arte. A maior parte dos seus trabalhos abordam um tema mais crítico, onde costuma a questionar alguns aspectos construídos pela sociedade atual.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/kQvPPnuzq3Q" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>
