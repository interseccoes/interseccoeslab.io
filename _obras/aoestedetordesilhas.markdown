---
layout: obra
thumbnail: /assets/tumbnail/t-aoestedetordesilhas.png
title: À oeste de Tordesilhas
artista: Adriana Teixeira
turma: Materiais em Arte II
instagram: https://www.instagram.com/adrianatmachado
texto-descricao: Me coloco diante da câmera do computador e deixo minha imagem se misturar a uma imagem do solo. Me deixo mesclar à natureza de maneira virtual e ilusória. Ficciono um lugar que foi descrito anteriormente por Cildo Meireles, que não consta nos mapas oficiais e se encontra à oeste de Tordesilhas. Aqui nada é estéril. A vida é latente, do pantano surgem os vermes. Olho para mim mesma como parte dessa coisa orgânica que é a terra. O devir toma meu corpo&#58; não a teoria do bom selvagem, não ao pensamento cartesiano. Antes o escatológico do que o estéril.
bio: Adriana Teixeira nasceu em 1999, é estudante de Teoria, Crítica e História da Arte pela Universidade de Brasília - UnB e uma das produtoras da zine SG1&#58;  Circular a Arte (https://www.instagram.com/circularaarte). Já participou das exposições Rabistência&#58; Rabiscando a Existência, Arte em Processo, (In)Tangível&#58; Matéria Sensível e Raízes. Procura criar a partir das relações de memória, afetos, tempo, identidade e as possibilidades históricas.

---

<img src="/assets/obras/materiais/adriana/imagem1.png"  alt="{{ page.title }}" class="img-fluid">


<div class="d-block d-sm-none">
  <img src="/assets/obras/materiais/adriana/imagem1.png"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
</div>
