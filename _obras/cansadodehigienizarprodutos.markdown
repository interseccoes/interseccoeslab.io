---
layout: obra
thumbnail: /assets/tumbnail/t-cansadodehigienizarprodutos.png
turma: Arte Eletrônica
title: Cansado de higienizar produtos
artista: Pedro Henrique Nascimento aka Feio
instagram: https://www.instagram.com/neguinho_feio
texto-descricao: Edição de vídeo experimental bem simples, que mostra o quão chato e repetitivo é essa nova rotina pela qual todos estão tendo que passar no momento. O ritual de higienização de se preparar uma solução de água com água sanitária; lavar frutas, verduras e legumes em água corrente, e embalagens com álcool 70 é extremamente monótono e exaustivo, porém indispensável nos dias atuai
bio: Sou um artista independente negro que trabalha com fotografia, gravura, desenho e pintura. Minha maior influência artística vem da vivência e experiência que obtive das ruas, que por sua vez é palco para alguns dos meus trabalhos. Sou de Brasília e atualmente estudo artes visuais na UnB.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/9GpDMCnVV9I" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </p>