---
layout: obra
thumbnail: /assets/tumbnail/t-imagemsemelhanca.png
title: Imagem semelhança - releitura de Adão e Eva numa tentativa pedagógica de se pensar não-binariedade
artista: Lídice Silveira
turma: Materiais em Arte II
instagram: https://www.instagram.com/lidiceonline
texto-descricao: Coexistindo na minha dualidade, sou do signo de gêmeos. Ao mesmo tempo, me apropriando da iconografia cristã pra questionar a binariedade. Deus não tem gênero, embora imaginado como um ancião branco barbado. Os papéis de gênero são obviamente construções sociais impostas pelo pensamento colonial patético e branco. Vc não é o que é, vc só está seguindo um roteiro que não é seu.
bio: Lídice, 1996, Brasília, Brasil.Nasceu, cresceu e trabalha no Distrito Federal. Lídice pesquisa, em multimídias, sobre experiências místicas, atmosferas internas e o caos.  A artista está no período de graduação em licenciatura em Artes Visuais, na Universidade de Brasília. Sua trajetória se faz em espaços pelo Distrito Federal e entorno, como no Espaço Piloto, pela curadoria de Yná Kabe Rodríguez, na exposição PALAVRA ANIMAL NÃO DOMÉSTICO, em 2019. Participou também do Inferninho Katya Flavya 2.0, com curadoria do Culto das Malditas, pela Pilastra (DF), e na mostra virtual lista, com curadoria de Mariana Destro, pela Guava Gallery, todas no mesmo ano.
---
<img src="/assets/obras/materiais/lidice/imagem1.jpeg"  alt="{{ page.title }}" class="img-fluid">

<div class="d-block d-sm-none">
  <img src="/assets/obras/materiais/lidice/imagem1.jpeg"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
</div>