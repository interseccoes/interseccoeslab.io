---
layout: obra
thumbnail: /assets/tumbnail/t-naomesigaspoisestouperdida.png
turma: Arte Eletrônica
title: Não me sigas pois estou perdida
artista: Luiz Ferreira
instagram: https://www.instagram.com/elefantesbrancos
texto-descricao: A videoarte é uma investigação sobre novelas televisivas e seus dramas, romances, vilãs e mundos fantasiosos de entretenimento em massa. A produção percorre entre o kitsch e o experimento entre ferramentas digitais para recriar cenas cinematográficas que trazem uma relação da metáfora e a produção latente nos sistemas televisivos.<br><br>Ficha técnica&#58;<br>captação&#58; Luiz Ferreira e LUA<br>Som&#58; LUA<br>Elenco&#58; Clara Maria Matos e Zifa Borges<br>
bio: Luiz Ferreira é artista visual graduando em Artes Visuais pela Universidade de Brasília. Em suas produções expõe as relações do tempo, espaço e memória promovendo uma experiência da reminiscência através de registros fotográficos e intervenções com a materialidade.

---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/lnBCgWzvpsA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </p>