---
layout: obra
thumbnail: /assets/tumbnail/t-queridodiario.png
title: querido diário 
artista: Ângelo Delico Nunes 
turma: Materiais em Arte II
instagram: https://www.instagram.com/cruelseraph
texto-descricao: Obras sobre pensamentos do dia a dia na forma de páginas de diário sobrepostas a flores prensadas ou cartas de um tarot imaginário.
bio: Ângelo Delico Nunes nasceu em Salvador em 1996 e se mudou para Brasília em 2006. Veterano do curso de artes visuais-bacharelado da UnB, apaixonado pelo desenho e pela arte no geral.
---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/materiais/angelo/imagem1.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/angelo/imagem2.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/angelo/imagem3.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/angelo/imagem4.png" alt="{{ page.title }}" class="img-fluid">
      </div>
    </div>
    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/materiais/angelo/imagem1.png"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/materiais/angelo/imagem2.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/angelo/imagem3.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/angelo/imagem4.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
</div>
