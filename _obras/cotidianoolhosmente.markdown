---
layout: obra
thumbnail: /assets/tumbnail/t-cotidianoolhosmente.png
turma: Arte Eletrônica
title: Cotidiano olhos mente
artista: Mariana Rezende Monteiro
instagram: https://www.instagram.com/mrm_art_
texto-descricao: O que os olhos captam? O que a mente guarda? Quais pensamentos permanecem? Às vezes as coisas podem ficar muito confusas, principalmente em tempos assim, estranhos, que parecem não ter fim. São muitos pensamentos que vem ao mesmo tempo, pedindo espaço, ou melhor, lutando por espaço. O cotidiano se torna exaustivo.
bio: Mariana Rezende Monteiro nasceu em 2000 e é estudante de Artes Visuais (licenciatura) na UnB. Desenvolve várias experimentações no campo da arte, mas as técnicas que mais tem afinidade são a pintura e o desenho. Recentemente descobriu um grande interesse pelos registros do cotidiano e de devaneios através da vídeo arte, que são o foco de sua pesquisa no momento.
---
<p style="text-align:center;">
<iframe width="560" height="315" src="https://www.youtube.com/embed/YkWGkwEj9l8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> </p>