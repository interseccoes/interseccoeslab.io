---
layout: obra
thumbnail: /assets/tumbnail/t-sonhos.png
turma: Arte Eletrônica
title: Sonhos
artista: Ruana Carla Santiago de Andrade
instagram: https://www.instagram.com/ruanita.c
texto-descricao: As obras são baseadas nos próprios sonhos da artista, criando texturas, sensações e questionamentos com as imagens.
bio: Artista mulher preta, independente, periférica e estudante de artes visuais pela Universidade de Brasília (UnB). Minhas referencias são minhas vivencias, minha comunidade e amigos, busco sempre trazer reflexões, curiosidade e inquietações com os meus trabalhos, uso diversas técnicas e linguagens, meu objetivo é causar questionamentos com imagens enigmáticas.
---
<div class="menu d-none d-sm-block">

<img src="/assets/obras/ae/ruana/imagem1.png"  alt="{{ page.title }}" class="img-fluid" ><br><br>

<p style="font-size: 12px;color: #1a33ea;">"O sonho é uma manifestação criativa da psique (mente inconsciente e
consciente) e transcende os meros cinco sentidos. Portanto, os sonhos podem, de forma simbólica e numa linguagem própria, revelar questões de sua personalidade que precisam ser trabalhadas."<br><br>
</p>
<p>Era uma sala sem portas ou janelas, muitas luzes artificiais, cheiro de sal, eu não sabia como havia entrado ali... No chão tinha uma bacia cheia de cobras, elas estavam muito agitadas e pareciam ferver lá dentro, das paredes desciam água e o quarto começou a encher, quanto mais água descia mais cobras apareciam, a água começou a se transformar em cobra ou as cobras começaram a virar água... Senti como se estivesse dentro de uma caixa de sapatos, cobras começaram a cair de mim e eu estava paralisada de medo, não conseguia mais controlar o meu corpo, quando me dei conta eu não era mais humana e sim uma cobra dentro da bacia, era apertado, gosmento e gelado... De alguma forma eu senti que tinha me transmutado e que agora eu era todas as cobras ao mesmo tempo, senti que eu estava fragmentada mas todas as pequenas partes de mim formavam um organismo maior.</p>

<img src="/assets/obras/ae/ruana/imagem2.png"  alt="{{ page.title }}" class="img-fluid" ><br><br>

<p style="font-size: 12px;color: #1a33ea;">“ A função geral dos sonhos é tentar restabelecer a nossa balança psicológica, produzindo um material onírico que reconstitui, de maneira sutil, o equilíbrio psíquico total, É ao que chamo função complementar (ou compensatória) dos sonhos na nossa constituição psíquica.’’ Carl Jung <br><br></p>

<p>Eu estava na China, as pessoas falavam mas suas bocas não se mexiam era como se toda a comunicação fosse feita telepaticamente... Eu era uma mulher chinesa com muitos dentes na boca e estava em trabalho de parto, por algum motivo eu odiava a situação, sentia que aquele bebê não me pertencia e tentei a todo custo que ninguém soubesse daquela criança... Estava deitada delirando de dor, o bebê nasceu... era um hibrido de coelho com humano e aquilo me deixou feliz (?) lembro de sentir uma lagrima escorrendo pelo meu rosto, toda dor tinha desaparecido, minha visão ficou turva, ouvi várias vozes de desespero na minha cabeça, eu estava em um estado de êxtase muito muito grande e então percebi que eu tinha morrido... acordei.</p>

<img src="/assets/obras/ae/ruana/imagem3.png"  alt="{{ page.title }}" class="img-fluid" ><br><br>

<p style="font-size: 12px;color: #1a33ea;">“A dificuldade em entender a imagística do sonho não ocorre porque o sonho esteja ocultando alguma coisa e sim porque pensamentos e emoções foram traduzidos em imagens, e porque a função do sonho é comunicar um conteúdo que estava anteriormente ausente da consciência.” MATTOON <br><br></p>

<p>Eu estava viajando para uma cidade do interior com alguns amigos, nesse lugar vivia uma tribo indígena isolada que ninguém conseguia ver, era noite na cidade e fomos a um rio, três trovões no céu, cigarras cantam, cheiro de madeira molhada... Flashes de uma cerimônia de consagração do daime, sai pela mata sozinha, as árvores eram pessoas, vi a tribo indígena, cada árvore era um indígena, cada indígena era uma árvore, eu estava dentro de um carro, esse carro começa a encher de água, a floresta começa a encher de água, vi animais submersos, vi muitos olhos, os animais também eram pessoas e eu não era mais eu, de repente eu virei uma mera observadora daquela situação confusa, nenhum pensamento ou atitude interferia naquele momento, percebi que estava sonhando e aquele sonho não parecia meu.</p>


</div>

<div class="d-block d-sm-none">

<img src="/assets/obras/ae/ruana/imagem1.png"  alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>

<p style="font-size: 12px;color: #1a33ea;">"O sonho é uma manifestação criativa da psique (mente inconsciente e
consciente) e transcende os meros cinco sentidos. Portanto, os sonhos podem, de forma simbólica e numa linguagem própria, revelar questões de sua personalidade que precisam ser trabalhadas."<br><br>
</p>
<p>Era uma sala sem portas ou janelas, muitas luzes artificiais, cheiro de sal, eu não sabia como havia entrado ali... No chão tinha uma bacia cheia de cobras, elas estavam muito agitadas e pareciam ferver lá dentro, das paredes desciam água e o quarto começou a encher, quanto mais água descia mais cobras apareciam, a água começou a se transformar em cobra ou as cobras começaram a virar água... Senti como se estivesse dentro de uma caixa de sapatos, cobras começaram a cair de mim e eu estava paralisada de medo, não conseguia mais controlar o meu corpo, quando me dei conta eu não era mais humana e sim uma cobra dentro da bacia, era apertado, gosmento e gelado... De alguma forma eu senti que tinha me transmutado e que agora eu era todas as cobras ao mesmo tempo, senti que eu estava fragmentada mas todas as pequenas partes de mim formavam um organismo maior.</p>

<img src="/assets/obras/ae/ruana/imagem2.png"  alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>

<p style="font-size: 12px;color: #1a33ea;">“ A função geral dos sonhos é tentar restabelecer a nossa balança psicológica, produzindo um material onírico que reconstitui, de maneira sutil, o equilíbrio psíquico total, É ao que chamo função complementar (ou compensatória) dos sonhos na nossa constituição psíquica.’’ Carl Jung <br><br></p>

<p>Eu estava na China, as pessoas falavam mas suas bocas não se mexiam era como se toda a comunicação fosse feita telepaticamente... Eu era uma mulher chinesa com muitos dentes na boca e estava em trabalho de parto, por algum motivo eu odiava a situação, sentia que aquele bebê não me pertencia e tentei a todo custo que ninguém soubesse daquela criança... Estava deitada delirando de dor, o bebê nasceu... era um hibrido de coelho com humano e aquilo me deixou feliz (?) lembro de sentir uma lagrima escorrendo pelo meu rosto, toda dor tinha desaparecido, minha visão ficou turva, ouvi várias vozes de desespero na minha cabeça, eu estava em um estado de êxtase muito muito grande e então percebi que eu tinha morrido... acordei.</p>

<img src="/assets/obras/ae/ruana/imagem3.png"  alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>

<p style="font-size: 12px;color: #1a33ea;">“A dificuldade em entender a imagística do sonho não ocorre porque o sonho esteja ocultando alguma coisa e sim porque pensamentos e emoções foram traduzidos em imagens, e porque a função do sonho é comunicar um conteúdo que estava anteriormente ausente da consciência.” MATTOON <br><br></p>

<p>Eu estava viajando para uma cidade do interior com alguns amigos, nesse lugar vivia uma tribo indígena isolada que ninguém conseguia ver, era noite na cidade e fomos a um rio, três trovões no céu, cigarras cantam, cheiro de madeira molhada... Flashes de uma cerimônia de consagração do daime, sai pela mata sozinha, as árvores eram pessoas, vi a tribo indígena, cada árvore era um indígena, cada indígena era uma árvore, eu estava dentro de um carro, esse carro começa a encher de água, a floresta começa a encher de água, vi animais submersos, vi muitos olhos, os animais também eram pessoas e eu não era mais eu, de repente eu virei uma mera observadora daquela situação confusa, nenhum pensamento ou atitude interferia naquele momento, percebi que estava sonhando e aquele sonho não parecia meu.</p>


</div>