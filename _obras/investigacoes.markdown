---
layout: obra
thumbnail: /assets/tumbnail/t-investigacoes.png
title: Investigações
artista: Alana Avohay
turma: Introdução à Gravura
instagram: https://www.instagram.com/vohay
texto-descricao:  Estimulada a experimentar, a artista se submeteu a descoberta de novos métodos e novos materiais. Em seus primeiros contatos com a gravura, resolveu procurar por objetos inesperados, como uma bolinha de papel alumínio enrolada em forma de carimbo, com tinta acrílica branca fosca, guache dourada e óleo azul. Além disso, o uso de carimbos de borracha, rolos de gravura e marcas de escova de dente. Tamanho de 21,0 x 29,7 cm em folha preta, e, tela de 30 x 30 cm.
bio: A artista Alana Avohay nasceu em Brasília no ano de 2000, sempre teve admiração pelas artes e costumava praticar desenhos em casa. Em sua adolescência se dedicou aos estudos de pintura a óleo e em 2019 ingressou na faculdade de Artes Visuais, na Universidade de Brasília. Agora, em seu terceiro semestre, se desafia a aprender novos conceitos e novas formas de praticar arte.
---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      <li data-target="#imagens" data-slide-to="5"></li>
      <li data-target="#imagens" data-slide-to="6"></li>
      <li data-target="#imagens" data-slide-to="7"></li>
      <li data-target="#imagens" data-slide-to="8"></li>


    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/gravura/alana/imagem1.jpeg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/alana/imagem2.jpeg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/alana/imagem3.jpeg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/alana/imagem4.jpeg" alt="{{ page.title }}" class="img-fluid">
      </div>
     <div class="carousel-item">
        <img src="/assets/obras/gravura/alana/imagem5.jpeg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/alana/imagem6.jpeg" alt="{{ page.title }}" class="img-fluid">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/gravura/alana/imagem7.jpeg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/alana/imagem8.jpeg" alt="{{ page.title }}" class="img-fluid">
      </div>
      </div>
       

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/gravura/alana/imagem1.jpeg"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/gravura/alana/imagem2.jpeg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/alana/imagem3.jpeg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/alana/imagem4.jpeg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/alana/imagem5.jpeg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/alana/imagem6.jpeg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/alana/imagem7.jpeg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/alana/imagem8.jpeg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
 
</div>
