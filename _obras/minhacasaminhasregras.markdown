---
layout: obra
thumbnail: /assets/tumbnail/t-minhacasaminhasregras.png
title: Minha casa, Minhas Regras
artista: Jonas Gabriel Flores Matos 
turma: Materiais em Arte II 
instagram: https://www.instagram.com/ego.meu
texto-descricao: Fotografias de brinquedos, as quais brincam com cores, composições e pensamentos. Elas se divertem invertendo as situações que acontecem sem dever acontecer…Violência contra quem violenta é legítima defesa. É a forma de não perpetuar as injustiças que foram normalizadas.
bio: Jonas Gabriel Flores Matos nasceu em 2000 e é estudante de Artes Visuais (licenciatura) na UnB. Suas obras transitam por várias técnicas, porém se interessa mais pelo desenho, fotografia e pintura. A maioria de suas composições envolve cores bastante saturadas e contrastantes. Busca apresentar um pouco de humor e afeto em sua arte, porque em grande parte do tempo, nós humanos, nos tornamos ilhas de tédio. Existe remédio mais eficaz que a arte?

---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators" style="right: 200px;">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      


    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/materiais/jonas/imagem1.jpeg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/jonas/imagem2.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/jonas/imagem3.jpeg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/jonas/imagem4.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
     <div class="carousel-item">
        <img src="/assets/obras/materiais/jonas/imagem5.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      </div>
       
    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon" style="background-image: url(&quot;data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='%23000' width='8' height='8' viewBox='0 0 8 8'%3e%3cpath d='M5.25 0l-4 4 4 4 1.5-1.5L4.25 4l2.5-2.5L5.25 0z'/%3e%3c/svg%3e&quot;);"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next" style="right: 200px;">
      <span class="carousel-control-next-icon" style="background-image: url(&quot;data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='%23000' width='8' height='8' viewBox='0 0 8 8'%3e%3cpath d='M2.75 0l-1.5 1.5L3.75 4l-2.5 2.5L2.75 8l4-4-4-4z'/%3e%3c/svg%3e&quot;);"></span>
    </a>
  
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/materiais/jonas/imagem1.jpeg"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/materiais/jonas/imagem2.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/jonas/imagem3.jpeg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/jonas/imagem4.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/jonas/imagem5.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>

 
</div>
