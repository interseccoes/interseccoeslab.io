---
layout: obra
thumbnail: /assets/tumbnail/t-semtitulo.png
title: Sem Título
artista: Isadora Cruz
turma: Introdução à Gravura
instagram: https://www.instagram.com/_.i.a.s.a._
texto-descricao: A artista buscou recursos alternativos para atingir o efeito do linóleo, chegando até o laqueado. A impressão foi realizada manualmente com uso de uma colher de madeira.
bio: A artista Isadora Cruz nasceu em Brasília no ano de 2001, ingressou na faculdade de Artes Visuais na Universidade de Brasília em 2019. Seu trabalho atual questiona o lugar da arte no espaço individual, buscando entrelaçar a arte com a experiência, propondo a aproximação entre a arte e a vida.

---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>


    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/gravura/isadora/imagem1.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/isadora/imagem2.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/isadora/imagem3.png" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/isadora/imagem4.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      </div>
   
       

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/gravura/isadora/imagem1.jpg"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/gravura/isadora/imagem2.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/isadora/imagem3.png" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/isadora/imagem4.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
 
 
</div>
