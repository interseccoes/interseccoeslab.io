---
layout: obra
thumbnail: /assets/tumbnail/t-sombrasdeumamanhanaoexistente.png
title: Sombras De Um Amanhã Não Existente 
artista: Catarina Leite de Matos e Almeida
turma: Introdução à Gravura 
instagram: 
texto-descricao: Fazendo o uso da técnica do estêncil com a junção da fotografia a obra visa criar sombras deformadas de imagens que retratam um possível fim apocalíptico para a pandemia atual, onde os animais sofreram mutações e os humanos já não existem mais 
bio: Catarina Leite de Matos e Almeida nasceu em 2001, em Salvador - BA, e atualmente está cursando Artes Visuais Licenciatura na Universidade de Brasília (UNB). Apresenta mais afinidade com técnicas mais voltadas para o desenho, mas também apresenta interesse em fazer uso de técnicas como a fotografia e a vídeo arte. A maior parte dos seus trabalhos abordam um tema mais crítico, onde costuma a questionar alguns aspectos construídos pela sociedade atual.


---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      <li data-target="#imagens" data-slide-to="5"></li>
      <li data-target="#imagens" data-slide-to="6"></li>


    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/gravura/catarina/imagem1.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/catarina/imagem2.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/catarina/imagem3.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/catarina/imagem4.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
     <div class="carousel-item">
        <img src="/assets/obras/gravura/catarina/imagem5.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/gravura/catarina/imagem6.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      </div>
   
       

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/gravura/catarina/imagem1.jpg"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/gravura/catarina/imagem2.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/catarina/imagem3.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/catarina/imagem4.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/catarina/imagem5.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/gravura/catarina/imagem6.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
 
</div>
