---
layout: obra
thumbnail: /assets/tumbnail/t-pequenosrearranjos.png
title: Pequenos Rearranjos
artista: Álvaro C. de Santana
turma: Materiais em Arte II
instagram: https://www.instagram.com/varoalll
bio: Alvaro de Santana (1993) é artista visual, graduando em Artes Visuais na Universidade de Brasília (UnB). Enquanto artista periférico, utiliza de seus processos de maneira a falar sobre o lugar do qual veio e a realidade do que chama de Distrito Federal profundo, lugares que supostamente ficam à sombra da utopia realizada que é Brasília e as consequências de sua criação, que gerou novas cidades à margem da capital, como Taguatinga ou Ceilândia. Investiga e trabalha com assemblagens de objetos de descarte coletados, calcogravura, pintura, desenho e vídeo. Realizou a exposição individual Expo-Pretexto em 2018, na galeria Olho de Águia (DF). Participa de exposições coletivas desde 2016, entre elas BURAQUERA, no Objeto Encontrado (DF), Dos Recortes às Micro-Invasões, no Espaço Cultural Kaixa D’Água (DF), Além da Forma, na Casa do Cantador (DF) e Retina, no Museu Nacional da República (DF).
texto-descricao: Dentro de um processo de acumulação de pequenos objetos, supostamente sem propósito, e outros que tiveram sua utilidade retirada ou aposentada, criei a obra objeto Pequenos Rearranjos. A acumulação das partes que compõem a obra se deu e se dá ao longo de alguns anos, quase que como mania, e quando eu faço a coleta de alguma coisa, essa coisa não necessariamente terá função artística de imediato, ficando guardada estaticamente apenas com a função de bibelô em uma estante. Em  decorrência do tempo de “cura”, por assim dizer, e do estro de criação, esses objetos são inseridos no processo de criação da obra. Pequenos Rearranjos, então, é o processo de transformação a partir do rearranjo desses outros objetos menores em obra.

---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
      <li data-target="#imagens" data-slide-to="5"></li>
      <li data-target="#imagens" data-slide-to="6"></li>
      <li data-target="#imagens" data-slide-to="7"></li>
      <li data-target="#imagens" data-slide-to="8"></li>
      <li data-target="#imagens" data-slide-to="9"></li>
      <li data-target="#imagens" data-slide-to="10"></li>


    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/materiais/alvaro/imagem1.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/alvaro/imagem2.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/alvaro/imagem3.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/alvaro/imagem4.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
     <div class="carousel-item">
        <img src="/assets/obras/materiais/alvaro/imagem5.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/alvaro/imagem6.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/materiais/alvaro/imagem7.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/materiais/alvaro/imagem8.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/materiais/alvaro/imagem9.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      <div class="carousel-item">
        <img src="/assets/obras/materiais/alvaro/imagem10.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       </div>

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/materiais/alvaro/imagem1.jpg"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
  <img src="/assets/obras/materiais/alvaro/imagem2.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/alvaro/imagem3.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/alvaro/imagem4.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/alvaro/imagem5.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/alvaro/imagem6.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/alvaro/imagem7.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/alvaro/imagem8.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/alvaro/imagem9.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
  <img src="/assets/obras/materiais/alvaro/imagem10.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
 
</div>
