---
layout: obra
thumbnail: /assets/tumbnail/t-naturezainfinita.png
title: Natureza infinita
artista: Matheus Antônio Vieira
turma: Materiais em Arte II
instagram:  https://www.instagram.com/chefmatheusvieira
texto-descricao: A obra consiste na representação do que seria a natureza sem fim, criticando as formas e meios pelo qual  estamos utilizando os recursos findáveis do meio ambiente e lembrando a fragilidade da palavra natureza e o quão fazemos parte dela. A utilização do pano manufaturado para produção da flor representa a transformação pela qual a natureza sofre por combater pragas,assim como nós também  estamos sofrendo combatendo pragas, como o corona que é uma pandemia para nós. Para a natureza, o homem é uma pandemia descontrolada e a melhor forma dela não ser detida, é sendo uma natureza infinita. 
bio: Matheus Vieira-2000- Brasília,Df. Formado em Gastronomia pela UCB  e estudante de Artes Visuais pela Universidade de Brasília. Atualmente, empreendedor na área de Gastronomia e nas horas vagas, produz  obras e aprimora as suas técnicas aprendidas durante o curso de Artes.

---

<div class="menu d-none d-sm-block">

  <div id="imagens" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#imagens" data-slide-to="0" class="active"></li>
      <li data-target="#imagens" data-slide-to="1"></li>
      <li data-target="#imagens" data-slide-to="2"></li>
      <li data-target="#imagens" data-slide-to="3"></li>
      <li data-target="#imagens" data-slide-to="4"></li>
    </ul>
    
    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="/assets/obras/materiais/matheus/imagem1.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/matheus/imagem2.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/matheus/imagem3.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
       <div class="carousel-item">
        <img src="/assets/obras/materiais/matheus/imagem4.jpg" alt="{{ page.title }}" class="img-fluid">
      </div>
      </div>
    
    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#imagens" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#imagens" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>
  </div>
</div>

<div class="d-block d-sm-none">
  <img src="/assets/obras/materiais/matheus/imagem1.jpg"  alt="{{ page.title }}" class="img-fluid" width="35%" ><br><br>
    <img src="/assets/obras/materiais/matheus/imagem2.jpg" alt="{{ page.title }}" class="img-fluid" width="35%"><br><br>
</div>


