---
title: Home
layout: default
---
<div class="container-fluid">
  <div class="row">
    

  {% for obra in site.obras %}
    
           <div class="card  col-lg-4 col-xl-2">
              <div class="box">
          <a  href="{{ BASE_PATH }}{{ obra.url | remove: '/index.html' }}">
                    <img src="{{ obra.thumbnail }}" /> 
                  </a>
              </div>
              
    </div>
    
  {% endfor %}
  </div>
</div>


 
