---
title: Turmas
layout: default
---
<div class="container-fluid">
  <div class="row">
<div class="card  col-lg-4 col-xl-2">
              <div class="box">
                  <a  href="/animacao">
                    <p>Animação </p> 
                  </a>
                  <p style="font-size: 12px;bottom: -4em;">prof. Artur Cabral</p>
              </div>
   </div>
   
   <div class="card  col-lg-4 col-xl-2">
              <div class="box">
                  <a  href="/arteeletronica">
                    <p>Arte Eletrônica I</p> 
                  </a>
                  <p style="font-size: 12px;bottom: -4em;">prof. Artur Cabral</p>
              </div>
   </div>
   
   <div class="card  col-lg-4 col-xl-2">
              <div class="box">
                  <a  href="/gravura">
                    <p>Introdução à Gravura</p> 
                  </a>
                  <p style="font-size: 12px;bottom: -4em;">profa. Andrea Campos</p>
              </div>
   </div>

   <div class="card  col-lg-4 col-xl-2">
              <div class="box">
                  <a  href="/materiais">
                    <p>Materiais em Arte II</p> 
                  </a>
                                    <p style="font-size: 12px;bottom: -4em;">profa. Lynn Carone</p>
              </div>
   </div>

  </div>
</div>

