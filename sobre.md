---
title: Sobre
layout: default
---

<div class="container-fluid">
  <div class="row">
    <div class="sobre col-sm-10">
      <h5>
    A exposição virtual INTERSECÇÕES é um evento do programa de extensão da
Universidade de Brasília, cujo propósito é mostrar o resultado de um semestre de
pesquisa dos discentes das disciplinas Arte Eletrônica 1, Animação, Introdução à
Gravura e Materiais em Arte 2.<br>
Desde o início do ano de 2020 foi preciso muita disposição para lidar com os
problemas causados pela pandemia da COVID-19. O primeiro deles foi a suspensão do
calendário acadêmico das universidades brasileiras, seguido do período de adaptação
dos docentes e discentes ao modo remoto de comunicação e aprendizagem,
modalidade de ensino que viabilizou a retomada das atividades acadêmicas no
contexto atípico imposto pelo isolamento social.<br>
A rotina das aulas foi intensa e as diversas atividades teórico-práticas exigiram dos
discentes novos meios e formas de pesquisa, além do desafio de manter o desejo de
seguir em frente apesar das limitações que o ensino remoto coloca.<br>
A exposição INTERSERCÇÕES é o resultado dos encontros semanais virtuais - aulas
síncronas e assíncronas – que oportunizaram discussões, apresentações de filmes,
leitura de textos e de obras de artistas referentes aos contextos discutidos, além de
tutorias com sugestões para o desenvolvimento dos trabalhos práticos. As trocas
foram ricas, promovendo o incentivo para a produção poética individual e a
aproximação entre os integrantes do grupo.<br>
Para a execução das obras foi preciso um novo olhar sobre a própria produção e lançar
mão tanto dos recursos materiais disponíveis ao alcance de cada um, quanto das
singularidades individuais, pois apesar de todas as limitações, o ambiente doméstico
se revelou um potente campo para a pesquisa poética e para a descoberta de novas
metodologias de trabalho.<br>
Podemos dizer que o semestre letivo de isolamento social reforçou a inegável
presença dos fenômenos tecnológicos no âmbito das novas perspectivas e
possibilidades estéticas. Nesse sentido, a exposição INTERSECÇÕES apresenta-se como
uma mostra dos processos de investigação poética no espaço virtual e das
intersecções entre as diferentes pesquisas realizadas ao longo do semestre. Ela nos
mostra também que mesmo diante de tantos desafios e desesperanças, a resiliência, a
resistência e a persistência ainda nos fazem crer no papel fundamental que a arte
cumpre.<br>
      </h5>
      <p class='nome-autor'>Andrea Campos de Sá<br>Artur Cabral<br>Lynn Carone</p>
    </div>
  </div>
</div>
