---
title: Performances
layout: default
---

<div class="container-fluid">
  <div class="row">
  <div class="performances col-sm-10">
  <h1>Performances</h1>
  <h2>De 19 a 22 de Outubro de 2020 no <a href="https://www.youtube.com/playlist?list=PLxkIqaMJsWqUY0zIjqVVt2kD4KPeWiynq">CANAL Medialab/UnB</a> (Youtube)</h2>
  <em>Cada performance dura entre 30 a 40 minutos (máximo).</em>
  <br/>
  <span>(!) Artistas vejam aqui as <a href="/transmissao">instruções para realizar transmissão</a> da performance.</span>

  <div class="row">
    <div class="col-sm-10">

  <h3>19/10 às 19h <i><a href="https://youtu.be/p6zDhyCQb2k" target="_blank">Sessão de Live Coding </a> com Pietro Bapthysthe</i></h3>
  <p>por Berin e Diego Dukão</p>
  <img width="100%" src="/assets/performances/pietro-bapthysthe.png" />
  <p style="text-align:justify">
  <br/>
  Pietro Bapthysthe é um duo brasileiro de live coding composto por Berin e Diego
  Dukão. Ambos vem explorando a programação criativa há mais de 2 anos. Entre
  estudos e experimentos, também desenvolveram ferramentas de software livre para
  artistas gráficos e sonoros. Em 2019, começaram a realizar performances sonoras
  e visuais praticando live coding utilizando as tecnologias FoxDot, Troop,
  SuperCollider e Processing. Além disso, já organizaram algumas edições de
  Algoraves no Brasil durante eventos de tecnologia como PythonBrasil e
  CriptoFesta de João Pessoa. Também são os organizadores da festa Algowave, em
  Recife, com o objetivo de promover espaços para novos artistas que exploram
  interseções entre novas sonoridades e tecnologia. Em 2020, também como
  prática-resposta ao estado pandêmico, começaram a gravar e lançar EPs mensais
  com 2 faixas como registros de gravações geradas com FoxDot. Em julho lançaram
  seu primeiro álbum, "Compyled", com um compilado remasterizado dos resultados
  dos primeiros seis meses desse tipo de prática.
  </p>
  <p><a href="https://pietrobapthysthe.bandcamp.com">https://pietrobapthysthe.bandcamp.com</a></p>


  <h3>20/10 às 19h <i><a href="https://youtu.be/DEGH9fUqQik" target="_blank">Pontos de in-re-flexão </a></i></h3>
  <p>por Jackson Marinho e Victor Valentim</p>
  <img width="100%" src="/assets/performances/pontos-de-inreflexao.jpg" />
  <p style="text-align:justify">
  <br/>
  A performance audiovisual Pontos de in-re-flexão é um sistema interativo
  telemático modelado por aprendizado de máquina. Composto pelos mídia artistas
  Jackson Marinho e Victor Valentim, e concebido como uma performance de dados
  sonoros e visuais, a experiência Pontos de in-re-flexão trafega entre as
  cidades de Cachoeira/BA e Taguatinga/DF na profusão de sons e imagens
  abstratas em constante retroalimentação.
  </p>
  <p style="text-align:justify">
  Jackson Marinho investiga as mídias digitais no horizonte do audiovisual e da
  interatividade para performances e instalações artísticas. Atualmente é
  doutorando pelo Programa de Pós-graduação em Artes Visuais na Universidade de
  Brasília e dentre as principais exposições e eventos que participou estão o
  FILE (Festival Internacional de Linguagem Eletrônica), FAD (Festival de Arte
  Digital) e o FONLAD#07 (Fonlad Digital Art Festival).
  <a href="http://webartes.net">http://webartes.net</a>
  </p>
  <p style="text-align:justify">
  Victor Valentim é professor Assistente em design de interfaces no CECULT -
  Centro de Cultura, Linguagens e Tecnologias Aplicadas da Universidade Federal
  do Recôncavo da Bahia (UFRB), Músico, Produtor Musical, Artista Multimídia,
  DJ, Pesquisador de música e novas tecnologias, graduado em composição musical
  e mestre em arte e tecnologia pela Universidade de Brasília (UnB). Atua no
  cenário da música e da arte tecnologia à 15 anos, com experiência em pesquisa
  e criação artística das relações entre o som e a imagem, com trabalhos
  apresentados no Brasil e diversos países. Fundador do selo musical
  Miniestéreo da Contracultura (<a href="http://miniestereo.org">miniestereo.org</a>)
  em 2012, atualmente integra o Lab0gam - UFRB (Laborátório de Gambiologia e
  Código Aberto) e desenvolve pesquisas de criação de obras para Fulldome
  (cinema para planetários), projeção mapeada em discos de vinil, música,
  tecnologia e astronomia.
  </p>


  <h3>21/10 às 20h<i><a href="https://youtu.be/sbF6BUwCzA4" target="_blank"> Janelas Afetivas</a></i></h3>
  <p>por Coletivo COM.6</p>
  <img width="100%" src="/assets/performances/janelas-afetivas.jpg" />
  <p style="text-align:justify">
  <br/>
  Coletivo COM.6 é Edilson Ferri, Agda Carvalho, Clayton Policarpo, Daniel
  Malva, Miguel Alonso e Sergio Venancio.  Um encontro sem roteiro e sem falas,
  uma experimentação de edições de vídeos, o encontro é simultaneamente
  traduzido em um mundo paralelo que desdobra-se em tempo real, mixada,
  trazendo elementos de outros conteúdos da internet e somente a vontade de
  estar junto, um compartilhamento de mundos íntimos.
  <p style="text-align:justify">Agda Carvalho: Artista Visual e Curadora. Pós Doutorado em Artes – IA Unesp. Doutora em Ciências da Comunicação (ECA-USP),  Estágio Pós Doutoral no Média Lab – UFG em Humanidades Digitais. Mestre em Artes Visuais (Instituto de Artes - UNESP). Membro do GIIP: Grupo Internacional e Interinstitucional de Pesquisa em Convergências entre Arte, Ciência e Tecnologia (UNESP). Docente do Curso de Design do Instituto Mauá de Tecnologia. E-mail: agdarcarvalho@gmail.com <br>
  Clayton Policarpo: Doutorando (bolsa Capes) e mestre em Tecnologias da Inteligência e Design Digital (TIDD), PUC-SP. integrante dos grupos de pesquisa TransObjetO (TIDD – PUC-SP) e Realidades (ECA – USP). E-mail: clayton.policarpo@gmail.com <br>
  Edilson Ferri: Artista e Arquiteto. Mestre em Poéticas Visuais (UNICAMP). Docente da Faculdade Impacta Tecnologia São Paulo.  Membro do GIIP: Grupo Internacional e Interinstitucional de Pesquisa em Convergências entre Arte, Ciência e Tecnologia (UNESP). E-mail edilsonferri@gmail.com <br>
  Daniel Malva: Artista visual e mestre no PPG em Artes do IA- Unesp, São Paulo. Membro do grupo de pesquisa c.A.t - ciência/ARTE/tecnologia e do GIIP – Grupo Internacional e Interinstitucional de Pesquisa em Convergências entre Arte, Ciência e Tecnologia (IA- Unesp). E-mail: info@malva.fot.br <br>
  Miguel Alonso: Artista plástico e arte educador, doutorando pelo PPGAV - ECA/USP. Mestre pelo PPG em Artes da UNESP. Educador de Tecnologias e Artes no SESC - São Paulo. Formado em bacharelado e em licenciatura em Artes Visuais na UNESP. Trabalha diretamente nas áreas de Multimídia, Gravura e desenvolvimento Tridimensional.  E-mail: miguelalonso@usp.br <br>
  Sergio José Venancio Júnior: Doutorando em Poéticas Visuais (ECA USP), bolsista CAPES Proex. Mestre em Artes Visuais (ECA USP). Membro do Grupo de Pesquisa Realidades (ECA – USP). Docente do Curso de Especialização em Design Gráfico (IA Unicamp) e da Pós-Graduação em Arquitetura Digital (Belas Artes SP). E-mail: svenancio@gmail.com
  </p>


  <h3>22/10 às 19h <i><a href="https://youtu.be/uvXasrmGIuo" target="_blank">A revolução dos bichos</a></i></h3>
  <p>por Nômade Lab</p>
  <img width="100%" src="/assets/performances/nomade1.jpg" />
  <p style="text-align:justify">
  <br/>
  Nômade Lab é Jackson Marinho (BR), Joenio Costa (BR), Leandro Muñoz (COL),
  Lorena Ferreira (BR), Phillip Jones (UK) e Thales Grilo (BR).  Em meio ao
  apocalipse de nossa era, onde enfrentamos um vírus global, ditaduras e
  alienações sociais, queimadas devastadoras e garimpos que estão aniquilando o
  Pantanal e a floresta Amazônica, a humanidade e os animais pedem uma
  revolução para o século XXI. O Coletivo Nômade Lab traz em sua performance
  intitulada A revolução dos bichos, o grito de revolta manifestada pela
  natureza. Desde 2019 o coletivo Nômade Lab vem realizando performances
  sonoro-visuais imersivas na cidade de Brasília e cidades satélites do DF.
  Devido a situação pandêmica no decorrer de 2020, o Nômade Lab migrou suas
  performances para ambientes de experimentação e expressão musical através da
  internet. Por meio das plataformas online NINJAM e Flock, o grupo promove
  improvisações que são transmitidas ao vivo. As performances integram
  ferramentas projetadas para atuarem em tempo real na geração de som e imagem,
  como os softwares para livecoding Sonic Pi, Hydra e TidalCycles, bem como os
  softwares de som e video Ableton Live, Reaktor, FL, Reaper, Sunvox e VDMX,
  além de instrumentos desenvolvidos por meio de prototipagem eletrônica
  (Arduinos), criados pelos próprios integrantes. 
  </p>
  <p style="text-align:justify">
  Nômade Lab é um coletivo de arte sonoro-visual com foco em improvisação ao
  vivo mesclando sons e imagens criados através de computadores (utilizando
  linguagens de programação para live coding como TidalCycles, SuperCollider,
  Sonic Pi, Pure Data e Hydra), ferramentas como VCV Rack, LMMS, Sound Vox,
  Processing, e instrumentos elétricos-eletrônicos construídos pelos
  participantes utilizando Arduino e outros recursos eletrônicos, assim como
  instrumentos tradicionais, como guitarra, baixo elétrico, e experimentações
  vocais. O grupo é formado por músicos, artistas e pesquisadores de diversas
  áreas do conhecimento, tendo relações estreitas com os laboratórios MediaLab
  UnB, Lappis UnB e Calango Hacker Clube, com o grupo BSBLOrk (Orquestra de
  Laptops de Brasília) e com as galerias de arte deCurators e MixMídia. Os
  participantes constantes são Joenio Costa (BR), Lorena Ferreira (BR), Thales
  Grilo (BR), Phillip Jones (UK), Jackson Marinho (BR) e Leandro Muñoz (COL). O
  coletivo Nômade Lab vem realizando várias performances desde 2019, que dentre
  elas são: Brasília Mapping Festival (2019), Exposição Rumor do Caixa Cultural
  Brasília (2020), Campus Party Brasília (2019) e Exposição Interfaces
  Computacionais Afetivas (2019) na cidade do Gama, Brasília.
  </p>
  <p style="text-align:justify"><a href="https://nomadelab.gitlab.io">https://nomadelab.gitlab.io</a></p>
  
  </div>
  </div>
</div>
