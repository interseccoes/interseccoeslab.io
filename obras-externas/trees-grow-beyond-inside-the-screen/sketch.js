//Colagens de códigos by Suzete Venturelli 2020

var branch = [];
var offset = -90.0;
var count;
var s_color;
var s_weight;

var instant = false;
var transparent;
var angles = 24;
var f = 1000;
var wind;
var trees = [];
var winddir = 1;


function startTouch(){
restart();
}


function setup() {

  pixelDensity (displayDensity ());
  createCanvas(windowWidth, windowHeight);
  //background(250,0,0);
  background (random(225),random(200),random(225));
  colorMode (RGB, 255, 255, 255, 100);
  //branch.push (new CreateBranch (width / 2, height, width / 2, height - 80.0, 80.0, 0.0));
  branch.push (new CreateBranch (width / 2, height, width / 2, height - 2.0, 180.0, 0.0));
  count = 0;//antes0
  s_color = 0;//antes0
  s_weight = 0;
  
  var a = new Tree(180,{end:""});
  a.start=createVector(width/2,height);
  a.length=150;
  a.angle=200;//180
  trees.push(a);
  wind=createVector(0,0);
  transparent=color(0,0,0,0);
  
}

function draw() {
 

  //for colado
  for (var i = 0; i < branch.length; i++) {
  branch[i].render ();
    branch[i].update ();
   }
  
  angleMode(RADIANS);
  
  //ventos
  winddir+=sin(frameCount/5000000000);
  wind.x=winddir;
  wind.x-=1000;
  wind.x/=120;
//angleMode(DEGREES);
//angleMode(RADIANS);
//background(250);//antes (0,25)
  fill(50,100,10);
  stroke(1);
  //text(winddir,150,150);
  
  for (var i = 0;i<trees.length;i++){
   var a = trees[i];
    a.upkeep();
    //line(a.start.x,a.start.y,a.end.x,a.end.y);
    //text(a.visibleLength+","+a.length,200,250);
    
  }
  //text(trees.length,200,200);
}

function CreateBranch (sx, sy, ex, ey, sl, sd) {
   
  var startx = sx;
  var starty = sy;
  var endx = ex;
  var endy = ey;
  var length = sl;
  var degree = sd;
  var nextx = startx;
  var nexty = starty;
  var prevx = startx;
  var prevy = starty;
  var next_flag = true;
  var draw_flag = true;
  this.update = function () {
    nextx += (endx - nextx) * 0.4;
    nexty += (endy - nexty) * 0.4;
    s_color = int (count / 2.0);
    //s_color = int (count / 10.0);
    s_weight = 2.0 / (count / 300 + 1);
    //s_weight = 3.0 / (count / 100 + 1);
    if (abs (nextx - endx) < 1.0 && abs (nexty - endy) < 1.0 && next_flag == true) {
      next_flag = false;
      draw_flag = false;
      nextx = endx;
      nexty = endy;
      //var num = int (random (2, 4));
      var num = int (random (1.0, 2.2));
      for (var i = 0; i < num; i++) {
        var sx = endx;
        var sy = endy;
        var sl = random (random (10.0, 20.0), length * 0.99);
        var sd = random (-24.0, 24.0);
        var ex = sx + sl * cos (radians (sd + degree + offset));
        var ey = sy + sl * sin (radians (sd + degree + offset));
        branch.push (new CreateBranch (sx, sy, ex, ey, sl, sd + degree));
      }
      count += 1;
    }
    if (branch.length > 6000) {
      count = 0;
      s_color = 0;
      s_weight = 0;
      var sx = random (width);//(width)
      var sl = random (0.0, 180.0);
      branch = [];
      branch.push (new CreateBranch (sx, height, sx, height - sl, sl, 0.0));
    }
  }
  this.render = function () {
    if (draw_flag == true) {
      stroke (s_color);
      strokeWeight (s_weight);
      line (prevx, prevy, nextx, nexty);
    }
    prevx = nextx;
    prevy = nexty;
  }
}
class Tree{
  constructor(angle,parent){
    this.end=createVector();
    this.start=parent.end;
    this.length=(100-parent.itter)/2+random(-10,10);
    this.visibleLength=0;
    this.angle = random(angle-25,angle+25);
    this.parent = parent.end;
    this.itter = parent.itter+1 || 0;
  }
  upkeep(){
    //this.start=this.parent;
    this.anglea=this.angle+wind.x
    this.end.x=this.start.x+sin(this.anglea)*this.visibleLength+wind.x;
    this.end.y=this.start.y+cos(this.anglea)*this.visibleLength+wind.y;
    line(this.start.x,this.start.y,this.end.x,this.end.y);
    //fill(255,0,0);
    //text(this.itter,this.start.x,this.start.y);
    //fill(255);
    /*rect(this.start.x,this.start.y,5,5);
    rect(this.end.x,this.end.y,5,5);*/
    if(this.visibleLength<this.length){
      this.visibleLength++;
      if(this.visibleLength===round(this.length)||instant){
       if(trees.length<f){
        trees.push(new Tree(this.angle+5*this.itter,this));
        trees.push(new Tree(this.angle-5*this.itter,this));
       }
        if(this.itter>5){
        trees.push(new Leaf(this));
       }
      }
    }
  }
}

class Leaf{
  constructor(parent){
    this.parent=parent;
    this.maxSize=random(15,30);
    this.size=3;
    this.age=2500;
    this.color=color(random(0,50),random(128,255),random(0,50),64);
    this.fcolor=color(random(0,255),random(0,128),random(0,50),64);
    //this.fcolor=color(random(0,255),random(0,255),random(0,255),64);
    this.angles=angles;//+random(8,8);
  }
  upkeep(){
    if(this.size<this.maxSize){
      this.size+=0.05;
    }
    this.age++;
    var angle = 360/this.angles;
    var c=this.color;
    if(this.age%5000>3000 && this.age%5000<4001){
      c=lerpColor(this.color,this.fcolor,(this.age-3000)/1000);
    }else if(this.age%5000>4000 && this.age%5000<4501){
      c=lerpColor(this.fcolor,transparent,(this.age-4000)/500);
    }else if(this.age%5000>4000){
      c=transparent;
    }
    if(this.age%5000===4999){
      this.size=0;
    }
    fill(c);
    stroke(0,0,0,0);
    push();
    translate(this.parent.end.x,this.parent.end.y);
    //rotate(this.age)
    beginShape();
    for(var i = 0;i<angles;i++){
      if(i%2===0){
      vertex(cos(i*angle)*this.size,sin(i*angle)*this.size);
      }else{
      vertex(cos(i*angle)*(this.size-3),sin(i*angle)*(this.size-3));
      }

    }
    endShape(CLOSE);
    pop();
    fill(255);
    stroke(255);
  }
}