ASSET_PREFIX = "";
SCRIPT_PREFIX = "";
SCENE_PATH = "1000181.json";
CONTEXT_OPTIONS = {
    'antialias': true,
    'alpha': false,
    'preserveDrawingBuffer': false,
    'preferWebGl2': true
};
SCRIPTS = [ 36078129, 36078136, 36078131, 36078126, 36078138, 36078140, 36078118, 36078128, 36078117, 36078124, 36078122, 36078121, 36078134, 36078751 ];
CONFIG_FILENAME = "config.json";
INPUT_SETTINGS = {
    useKeyboard: true,
    useMouse: true,
    useGamepads: false,
    useTouch: true
};
pc.script.legacy = false;
PRELOAD_MODULES = [
];
